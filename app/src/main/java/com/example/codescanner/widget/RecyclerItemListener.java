package com.example.codescanner.widget;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;

/**
 * Created by Mac on 15/5/6.
 */
public class RecyclerItemListener implements RecyclerView.OnItemTouchListener {
    public interface OnClickListener {
        void onClick(View v, int position);
    }

    private OnClickListener mListener;
    private GestureDetector mDetector;

    public RecyclerItemListener(Context context, OnClickListener listener) {
        mListener = listener;
        mDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
            @Override
            public boolean onSingleTapUp(MotionEvent e) {
                return true;
            }
        });
    }

    @Override
    public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
        View childView = rv.findChildViewUnder(e.getX(), e.getY());
        if (childView != null && mListener != null && mDetector.onTouchEvent(e)) {
            mListener.onClick(childView, rv.getChildLayoutPosition(childView));
        }
        return false;
    }

    @Override
    public void onTouchEvent(RecyclerView rv, MotionEvent e) {

    }
}
