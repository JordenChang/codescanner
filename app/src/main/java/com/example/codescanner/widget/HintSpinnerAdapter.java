package com.example.codescanner.widget;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by Mac on 15/4/17.
 */
public class HintSpinnerAdapter extends ArrayAdapter<String> {

    public HintSpinnerAdapter(Context context, int resource, int textViewResourceId, String[] objects) {

        super(context, resource, textViewResourceId, new ArrayList<>(Arrays.asList(objects)));
        setDropDownViewResource(resource);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = super.getView(position, convertView, parent);
        if (position == getCount()) {
            ((TextView) view).setText("");
            ((TextView) view).setHint(getItem(getCount()));
        }
        return view;
    }

    @Override
    public int getCount() {
        return super.getCount() - 1;
    }

    public void setHint(String hint) {
        add(hint);
    }

    /**
     * set spinner selection to the trail i.e. display hint
     *
     * @param spinner
     */
    public void showHint(Spinner spinner) {
        if (spinner.getAdapter() instanceof HintSpinnerAdapter) {
            spinner.setSelection(getCount());
        } else {
            throw new UnsupportedOperationException("Can not show hint on spinner which uses other types of adapter");
        }
    }

}
