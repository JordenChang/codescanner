package com.example.codescanner.widget;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Created by Mac on 15/7/31.
 */
public class ImageViewer {
    private static final String TAG = "image viewer";
    private AlertDialog mViewer;
    private ImageView mShowImage;
    private ArrayList<String> mPaths;
    private int mCurrIndex;
    private Context mContext;
    private class OnScrollListener implements View.OnTouchListener {
        private float mx, my, curX, curY;
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            switch (event.getAction()) {

                case MotionEvent.ACTION_DOWN:
                    mx = event.getX();
                    my = event.getY();
                    break;

                case MotionEvent.ACTION_UP:
                    curX = event.getX();
                    curY = event.getY();
                    if(curX-mx>0) {
                        Log.d(TAG, "last of " + mCurrIndex);
                        last();
                    } else {
                        Log.d(TAG, "next of " + mCurrIndex);
                        next();
                    }
//                    v.scrollBy((int) (mx - curX), (int) (my - curY));
                    break;
            }

            return true;
        }
    }

    public ImageViewer(Context context) {
        mContext = context;
        mShowImage = new ImageView(context);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT);
        mShowImage.setLayoutParams(lp);
        mShowImage.setOnTouchListener(new OnScrollListener());
        mShowImage.setScaleType(ImageView.ScaleType.FIT_CENTER);

        mViewer = new AlertDialog.Builder(context).setView(mShowImage).setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                System.gc();
            }
        }).create();

    }
    public void show(String path, ArrayList<String> paths) {
        show(paths.indexOf(path), paths);
    }
    private void show(int index, ArrayList<String> paths) {
        mPaths = paths;
        mCurrIndex = index;
        if(index>=0 && index < paths.size()) {
            mShowImage.setImageBitmap(BitmapFactory.decodeFile(mPaths.get(mCurrIndex)));
            if (!mViewer.isShowing()) {
                mViewer.show();
            }
        }
    }

    private void next() {
        if(mCurrIndex+1<mPaths.size()) {
            freeBitmap();
            show(++mCurrIndex, mPaths);
        } else {
            Toast.makeText(mContext, "已無下一張", Toast.LENGTH_SHORT).show();
        }
    }
    private void last() {
        if(mCurrIndex-1>=0) {
            freeBitmap();
            show(--mCurrIndex, mPaths);
        } else {
            Toast.makeText(mContext, "已無前一張", Toast.LENGTH_SHORT).show();
        }
    }

    private void freeBitmap() {
        ((BitmapDrawable)mShowImage.getDrawable()).getBitmap().recycle();
    }
}
