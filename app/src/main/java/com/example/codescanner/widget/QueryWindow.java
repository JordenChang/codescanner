package com.example.codescanner.widget;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.codescanner.MainActivity;
import com.example.codescanner.R;
import com.example.codescanner.request.SendingRequestUtils;
import com.example.codescanner.request.response.ImplResponseListener;
import com.example.codescanner.tools.BranchDataUtil;
import com.example.codescanner.tools.FavoriteOption;
import com.example.codescanner.tools.Observer;
import com.example.codescanner.tools.PreferenceHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Mac on 15/4/16.
 */
public class QueryWindow implements Observer, DialogInterface.OnClickListener {
    private static final String TAG = "query";
    private static final String TITLE = "存貨查詢";
    private FavoriteOption[] mBranches;
    private AlertDialog mResultDialog;
    private AlertDialog mQueryDialog;
    private ProgressBar mLoadingBar;
    private TextView mBarCodeText, shopName, itemName, purchaseNum, salesNum, stockNum, updateTime;
    private Spinner mBranchSpinner;
    private MainActivity mContext;
    private BranchDataUtil mUtil;
    private ArrayAdapter<FavoriteOption> mBranchAdapter;
    private ViewGroup mTextContainer;
    private ImplResponseListener mResponseListener;
    private ColorStateList mDefaultColors;

    public QueryWindow(MainActivity context) {
        mContext = context;

        // init alert dialog
        AlertDialog.Builder resultBuilder = new AlertDialog.Builder(context);
        AlertDialog.Builder queryBuilder = new AlertDialog.Builder(context);

        mUtil = new BranchDataUtil(context);
        mUtil.attach(this);

        //get branch data
        mBranches = mUtil.getOptions();

        View mResultView = LayoutInflater.from(context).inflate(R.layout.window_query_result, null);
        View queryView = View.inflate(context, R.layout.window_query, null);

        mLoadingBar = (ProgressBar) mResultView.findViewById(R.id.loading_progress);
        resultBuilder
                .setView(mResultView)
                .setTitle(TITLE)
                .setPositiveButton("確定", null);
        mResultDialog = resultBuilder.create();

        // init widget
        mBranchSpinner = (Spinner) queryView.findViewById(R.id.branch_spinner);
        mBarCodeText = (TextView) queryView.findViewById(R.id.barcode_text);

        mBranchAdapter = new ArrayAdapter<>(context, android.R.layout.simple_list_item_1, android.R.id.text1, mBranches);
        mBranchAdapter.setDropDownViewResource(android.R.layout.simple_list_item_1);
        mBranchSpinner.setAdapter(mBranchAdapter);

        queryBuilder
                .setView(queryView)
                .setTitle(TITLE)
                .setPositiveButton("送出", null)
                .setNegativeButton("取消", this)
                .setNeutralButton("掃描", this);
        mQueryDialog = queryBuilder.create();

        // custom function prevent from alert dialog dismiss immediately
        mQueryDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                Button positiveBtn = mQueryDialog.getButton(DialogInterface.BUTTON_POSITIVE);
                positiveBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        queryStock();
                    }
                });
            }
        });


        shopName = (TextView) mResultView.findViewById(R.id.branch_name_text);
        itemName = (TextView) mResultView.findViewById(R.id.item_name_text);
        purchaseNum = (TextView) mResultView.findViewById(R.id.purchase_num_text);
        salesNum = (TextView) mResultView.findViewById(R.id.shipping_num_text);
        stockNum = (TextView) mResultView.findViewById(R.id.stock_num_text);
        updateTime = (TextView) mResultView.findViewById(R.id.update_time_text);
        mTextContainer = (ViewGroup) mResultView.findViewById(R.id.text_container);

        mDefaultColors = shopName.getTextColors();

        mResponseListener = new ImplResponseListener(context) {
            @Override
            public void onSuccess(String result) {
                mBarCodeText.setText("");
                mLoadingBar.setVisibility(View.INVISIBLE);
                try {
                    JSONObject jResult = new JSONObject(result);
                    shopName.setText("分店：" + jResult.getString("shop"));
                    itemName.setText(jResult.getString("name"));
                    purchaseNum.setText("進貨：" + jResult.getString("purchase"));
                    salesNum.setText("銷貨：" + jResult.getString("sales"));
                    stockNum.setText("庫存：" + jResult.getString("stock"));

                    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm");
                    long update = Long.parseLong(jResult.getString("updateTime")) * 1000;
                    updateTime.setText("更新時間：" + format.format(new Date(update)));
                    if (System.currentTimeMillis() - update > 24 * 3600 * 1000) {
                        updateTime.setTextColor(Color.RED);
                    } else {
                        updateTime.setTextColor(mDefaultColors);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } finally {
                    mTextContainer.setVisibility(View.VISIBLE);
                }

            }

            @Override
            public void onClientError(String errorMsg) {
                super.onClientError(errorMsg);
                if (errorMsg.contains("not_found")) {
                    makeToast("找不到該商品");
                }
            }

            @Override
            public void onFail() {
                mLoadingBar.setVisibility(View.INVISIBLE);
                shopName.setText("分店：--");
                itemName.setText("--");
                purchaseNum.setText("進貨：--");
                salesNum.setText("銷貨：--");
                stockNum.setText("庫存：--");
                updateTime.setText("更新時間：--");
                updateTime.setTextColor(mDefaultColors);
                mTextContainer.setVisibility(View.VISIBLE);
            }
        };


    }

    public void showQueryWindow(String barcode) {
        if (mQueryDialog != null) {
            mQueryDialog.show();
            if (barcode != null && !barcode.isEmpty()) {
                mBarCodeText.setText(barcode);
            } else {
                mBarCodeText.setHint("請輸入或掃描商品條碼");
            }
        }
    }

    private void showResultWindow() {
        if (mResultDialog != null) {
            mResultDialog.show();
            if (mLoadingBar != null) {
                mLoadingBar.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        switch (which) {
            case DialogInterface.BUTTON_NEUTRAL:
                mContext.startScan();
        }
    }

    private void queryStock() {
        final String barcode = mBarCodeText.getText().toString();
        if (!barcode.isEmpty()) {
            final int select = mBranchSpinner.getSelectedItemPosition();
            Log.d(TAG, "select: " + select);

            mQueryDialog.dismiss();

            // add params
            Map<String, String> params = new HashMap<>();
            params.put("token", PreferenceHelper.getToken());
            try {
                Log.d(TAG, mBranches[select].getName());
                params.put("shopName", URLEncoder.encode(mBranches[select].getName().trim(), "UTF-8"));
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            params.put("barcode", barcode);

            SendingRequestUtils utils = new SendingRequestUtils("/stocks");
            utils.setUrlParams(params);
            utils.setResponseListener(mResponseListener);
            utils.execute(SendingRequestUtils.HTTP_GET);

            showResultWindow();
            mTextContainer.setVisibility(View.INVISIBLE);
        } else {
            Toast.makeText(mContext, "資料不完整", Toast.LENGTH_SHORT).show();
        }
    }

    public void dismiss() {
        if(mQueryDialog!=null && mQueryDialog.isShowing()) {
            mQueryDialog.dismiss();
        }
        if(mResultDialog!=null && mResultDialog.isShowing()) {
            mResultDialog.dismiss();
        }
    }


    @Override
    public void update(FavoriteOption[] data) {
        mBranches = data;
        mBranchAdapter.sort(new FavoriteOption.FavoriteComparator());
        mBranchAdapter.notifyDataSetChanged();
    }
}
