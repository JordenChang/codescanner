package com.example.codescanner;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;

import com.example.codescanner.tools.RelaunchFlag;

/**
 * Created by Mac on 15/5/8.
 */
public class UploadedActivity extends ActionBarActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_uploaded);
        RelaunchFlag.getInstance().setIsNeed(false);
        if(savedInstanceState==null) {
            getSupportFragmentManager().beginTransaction().replace(R.id.container, new UploadedFragment()).commit();
        }

    }
}
