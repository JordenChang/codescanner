package com.example.codescanner.board;

import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ZoomControls;

import com.example.codescanner.R;
import com.example.codescanner.request.UpdateImageUtils;
import com.example.codescanner.tools.EfficientBitmapHelper;
import com.example.codescanner.tools.SizeHelper;


import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Mac on 15/5/24.
 */
public class BoardContentActivity extends ActionBarActivity implements View.OnClickListener {
    private static final String TAG = "board content";
    private TextView mTitleText, mContentText, mTimeText;
    private ViewGroup mThumbnailList;
    private UpdateImageUtils mUpdateImageUtils;
    private boolean mFinish;
    private int[] mSize;
    private Map<String, ImageView> mImages = new HashMap<>();
    private AlertDialog mImageViewer;

    private class ImageHandler extends Handler {

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            if (!mFinish) {
                Bundle bundle = msg.getData();
                String hash = bundle.getString("hash");
                ImageView imageView = mImages.get(hash);

                boolean isSuccess = bundle.getBoolean("success");
                if (isSuccess) {

                    byte[] data = bundle.getByteArray("data");
                    if (data != null) {
                        imageView.setImageBitmap(BitmapFactory.decodeByteArray(data, 0, data.length));
                    }
                    imageView.setTag("-" + hash);

                } else {
                    imageView.setImageResource(R.drawable.ic_refresh);
                    imageView.setTag("!" + hash);
                }
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "on create");
        setContentView(R.layout.activity_board_content);
        mSize = SizeHelper.getScreenSize(this);
        mUpdateImageUtils = new UpdateImageUtils(this, new ImageHandler());
        Intent preIntent = getIntent();
        Bundle extras = preIntent.getExtras();
        if (extras != null) {
            mTimeText = (TextView) findViewById(R.id.time_text);
            mTitleText = (TextView) findViewById(R.id.title);
            mContentText = (TextView) findViewById(R.id.content_text);
            mThumbnailList = (ViewGroup) findViewById(R.id.thumbnail_list);

            mTimeText.setText(extras.getString("post_time"));
            mTitleText.setText(extras.getString("title"));
            mContentText.setText(extras.getString("content"));
            String[] hash = extras.getStringArray("hash");
            for (String aHash : hash) {
                Log.d(TAG, "get: " + aHash);
                addEmptyImage(aHash);
                mUpdateImageUtils.update(aHash);

            }
            mUpdateImageUtils.start();
        } else {
            finish();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mImageViewer != null && mImageViewer.isShowing()) {
            mImageViewer.dismiss();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "on destroy");
        mFinish = true;
        mUpdateImageUtils.interrupt();
        mImages.clear();
    }

    @Override
    public void onClick(View v) {
        Log.d(TAG, "click: " + v);
        if (v instanceof ImageView) {
            ImageView image = (ImageView) v;
            String tag = (String) image.getTag();
            Log.d(TAG, "click " + tag);

            // ! marks the image which is loaded fail.
            if (tag.startsWith("!")) {
                // Do reload.
                String hash = tag.substring(1);
                mUpdateImageUtils.update(hash);
                mUpdateImageUtils.start();

            } else if (tag.startsWith("-")) {
                String hash = tag.substring(1);
                int[] dimen = EfficientBitmapHelper.optionsMap.get(hash);
                Log.d(TAG, "dimen: " + Arrays.toString(dimen));
                zoomImage(((BitmapDrawable) image.getDrawable()).getBitmap());


            } else {
                Toast.makeText(this, "圖片載入中...", Toast.LENGTH_SHORT).show();
            }

        }
    }

    private void addEmptyImage(String hash) {
        ImageView imageView = new ImageView(this);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(mSize[0] / 5, mSize[0] / 5);
        imageView.setLayoutParams(lp);
        imageView.setPadding(5, 0, 5, 0);
        imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
        imageView.setClickable(true);
        imageView.setOnClickListener(BoardContentActivity.this);
        imageView.setImageResource(R.drawable.no_media);
        imageView.setTag(hash);
        mImages.put(hash, imageView);
        mThumbnailList.addView(imageView);
    }


    private void zoomImage(Bitmap bitmap) {
        View view = View.inflate(this, R.layout.window_zoom_image, null);
        final ImageView showImg = (ImageView) view.findViewById(R.id.content_img);
        ZoomControls zoom = (ZoomControls) view.findViewById(R.id.zoom);
        final RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        showImg.setLayoutParams(lp);
        showImg.setOnTouchListener(new View.OnTouchListener() {
            int prevX, prevY;
            boolean deJump;

            @Override
            public boolean onTouch(View v, MotionEvent event) {
//                    mScaleDetector.onTouchEvent(event);

                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        deJump = false;
                        prevX = (int) event.getRawX();
                        prevY = (int) event.getRawY();
                        lp.bottomMargin = -2 * v.getHeight();
                        lp.rightMargin = -2 * v.getWidth();
                        v.setLayoutParams(lp);
                        return true;
                    case MotionEvent.ACTION_MOVE:
                        if (!deJump) {
                            lp.topMargin += (int) event.getRawY() - prevY;
                            prevY = (int) event.getRawY();
                            lp.leftMargin += (int) event.getRawX() - prevX;
                            prevX = (int) event.getRawX();
                            v.setLayoutParams(lp);
                        }

                        return true;
                    case MotionEvent.ACTION_POINTER_UP:
                        deJump = true;
                        return true;
                    case MotionEvent.ACTION_POINTER_DOWN:
                        deJump = true;
                        return true;
                    default:
                        return false;
                }

            }
        });
        showImg.setImageBitmap(bitmap);

        zoom.setOnZoomOutClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                float x = showImg.getScaleX();
                float y = showImg.getScaleY();

                if (x > 1 && y > 1) {
                    showImg.setScaleX(x - 1);
                    showImg.setScaleY(y - 1);
                }
            }
        });
        zoom.setOnZoomInClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                float x = showImg.getScaleX();
                float y = showImg.getScaleY();
                showImg.setScaleX(x + 1);
                showImg.setScaleY(y + 1);

            }
        });


        mImageViewer = new AlertDialog.Builder(this).setView(view).create();
        mImageViewer.show();
    }

}
