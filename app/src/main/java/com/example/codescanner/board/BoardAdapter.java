package com.example.codescanner.board;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.codescanner.R;
import com.example.codescanner.tools.PreferenceHelper;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by Mac on 15/5/24.
 */
public class BoardAdapter extends RecyclerView.Adapter<BoardAdapter.LineHolder> {
    private static final String TAG = "board adapter";
    private List<BoardLine> mLines = new ArrayList<>();
    private SharedPreferences mReadInfo;
    private Context mContext;
    public BoardAdapter(Context context){
        mContext = context;
        PreferenceHelper.init(context);
        mReadInfo = PreferenceHelper.getReadInfo();
    }
    @Override
    public LineHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.layout_board_line, parent, false);
        return new LineHolder(view);
    }

    @Override
    public void onBindViewHolder(LineHolder holder, int position) {
        BoardLine line = mLines.get(position);
        String title = line.getTitle();

        if(PreferenceHelper.getReadInfo().getBoolean(title, false)) {
            holder.markRead();
        } else {
            holder.markUnread();
        }

        holder.mTitleText.setText(title);
        holder.mTimeText.setText(line.getTimeString());
    }

    @Override
    public int getItemCount() {
        return mLines.size();
    }

    public static class LineHolder extends RecyclerView.ViewHolder {
        TextView mTitleText, mTimeText;
        private Drawable defaultColor;
        public LineHolder(View itemView) {
            super(itemView);
            mTimeText = (TextView) itemView.findViewById(R.id.time_text);
            mTitleText = (TextView) itemView.findViewById(R.id.title);
            defaultColor = itemView.getBackground();
        }
        public void markUnread() {
            itemView.setBackgroundResource(R.drawable.selector_text_click_round_mark);
        }
        public void markRead() {
            itemView.setBackgroundResource(R.drawable.selector_text_click_round);
        }
    }

    public void addLine(BoardLine line) {
        mLines.add(line);
        notifyItemChanged(mLines.size()-1);
    }

    public BoardLine getLine(int i) {
        return mLines.get(i);
    }

    public boolean getUnread() {
        Map<String, ?> read = PreferenceHelper.getReadInfo().getAll();
        for(int i=0, len=mLines.size();i<len;i++) {
            if(!read.containsKey(mLines.get(i).getTitle())) {
                return true;
            }
        }
        return false;
    }
}
