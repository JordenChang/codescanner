package com.example.codescanner.board;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Mac on 15/5/24.
 */
public class BoardLine {
    private long mPostTime;
    private int mId;
    private String mTitle, mContent, mTimeString;
    private String[] mHash;
    public BoardLine(int id, String title, String content, String[] hash, long postTime) {
        this.mId = id;
        this.mTitle = title;
        this.mContent = content;
        mHash = hash;
        mPostTime = postTime;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm");

        mTimeString = sdf.format(new Date(postTime));
    }

    public String getTimeString() {
        return mTimeString;
    }
    public long getPostTime() {return mPostTime;}
    public int getId() {
        return mId;
    }


    public String getTitle() {
        return mTitle;
    }

    public String getContent() {
        return mContent;
    }

    public String[] getHash() {
        return mHash;
    }
}
