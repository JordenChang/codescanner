package com.example.codescanner.board;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.example.codescanner.MainActivity;
import com.example.codescanner.R;
import com.example.codescanner.request.SendingRequestUtils;
import com.example.codescanner.request.response.ImplResponseListener;
import com.example.codescanner.tools.PreferenceHelper;
import com.example.codescanner.widget.RecyclerItemListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Mac on 15/5/24.
 */
public class BoardListActivity extends ActionBarActivity implements RecyclerItemListener.OnClickListener {
    private static final String TAG = "board list";
    private RecyclerView mBoardList;
    private BoardAdapter mBoardAdapter;
    private SendingRequestUtils mRequestUtils;
    private LinearLayoutManager mLayoutManager;
    private boolean mLoad;
    private List<BoardLine> mBoardLines = new ArrayList<>();
    private ProgressDialog mProgress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_board_list);
        mBoardAdapter = new BoardAdapter(this);
        mProgress = new ProgressDialog(this);
        mProgress.setMessage("取得最新公告...");
        mProgress.setCancelable(false);

        mLayoutManager = new LinearLayoutManager(this);
        mBoardList = (RecyclerView) findViewById(R.id.board_list);

        mBoardList.setLayoutManager(mLayoutManager);
        mBoardList.setHasFixedSize(false);
        mBoardList.setAdapter(mBoardAdapter);
        mBoardList.addOnItemTouchListener(new RecyclerItemListener(this, this));

        //TODO add path
        mRequestUtils = new SendingRequestUtils("/board");
        Map<String, String> params = new HashMap<>();
        params.put("token", PreferenceHelper.getToken());
        params.put("since", "0");
        params.put("start", "0");
        mRequestUtils.setUrlParams(params);
        mRequestUtils.execute(SendingRequestUtils.HTTP_GET);
        mProgress.show();
        mRequestUtils.setResponseListener(new ImplResponseListener(this) {
            @Override
            public void onFail() {
                mLoad = false;
                Toast.makeText(BoardListActivity.this, "更新失敗", Toast.LENGTH_SHORT).show();

                dismissProgress();
            }

            @Override
            public void onSuccess(String result) {
                mLoad = false;
                dismissProgress();
                SharedPreferences.Editor editor = PreferenceHelper.getReadInfo().edit();
                long current = System.currentTimeMillis();
                try {

                    JSONArray jResult = new JSONArray(result);
                    int size = jResult.length();
                    for (int i = 0; i < size; i++) {
                        JSONObject jLine = jResult.getJSONObject(i);
                        String title = jLine.getString("title");
                        String content = jLine.getString("content");
                        int id = jLine.getInt("id");
                        JSONArray jImages = jLine.getJSONArray("images");
                        int len = jImages.length();
                        String[] images = new String[len];
                        for (int j = 0; j < len; j++) {
                            images[j] = jImages.optString(j);
                            Log.d(TAG, j + " / " + images[j]);
                        }
                        long time = jLine.getLong("postTime") * 1000;

                        // Consider inform which have posted 4 days is read.
                        if (current - time > 4 * 24 * 60 * 60 * 1000) {
                            editor.putBoolean(title, true);
                        }
                        mBoardAdapter.addLine(new BoardLine(id, title, content, images, time));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } finally {
                    editor.apply();
                }
            }
        });

        mBoardList.setOnScrollListener(new RecyclerView.OnScrollListener() {
            int pastVisibleItems,
                    visibleItemCount,
                    totalItemCount;

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {

                visibleItemCount = mLayoutManager.getChildCount();
                totalItemCount = mLayoutManager.getItemCount();
                pastVisibleItems = mLayoutManager.findFirstVisibleItemPosition();
                if (!mLoad) {
                    if ((visibleItemCount + pastVisibleItems) >= totalItemCount && totalItemCount % 15 == 0) {
                        mLoad = true;
                        int last = mBoardLines.size() - 1;
                        if (!mBoardLines.isEmpty()) {
                            BoardLine boardLine = mBoardLines.get(last);
                            Map<String, String> params = new HashMap<>();
                            params.put("token", PreferenceHelper.getToken());
                            params.put("since", String.valueOf(boardLine.getPostTime() / 1000));
                            params.put("start", String.valueOf(last));
                            mRequestUtils.setUrlParams(params);
                            mRequestUtils.execute(SendingRequestUtils.HTTP_GET);
                        } else {
                            mLoad = false;
                        }
                    }
                } else {
                    Toast.makeText(BoardListActivity.this, "資料載入中...", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    @Override
    protected void onRestart() {
        super.onRestart();
        mBoardAdapter.notifyDataSetChanged();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = new MenuInflater(this);
        inflater.inflate(R.menu.board, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.home_btn) {
            if (!mBoardAdapter.getUnread()) {
                Intent intent = new Intent(this, MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                return true;
            } else {
                Toast.makeText(this, "請先把公告看完", Toast.LENGTH_SHORT).show();
            }
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v, int position) {
        BoardLine line = mBoardAdapter.getLine(position);
        Bundle bundle = new Bundle();
        String title = line.getTitle();
        PreferenceHelper.getReadInfo().edit().putBoolean(title, true).apply();
        bundle.putString("title", title);
        bundle.putString("content", line.getContent());
        bundle.putString("post_time", line.getTimeString());
        bundle.putStringArray("hash", line.getHash());

        Intent intent = new Intent(this, BoardContentActivity.class);
        intent.putExtras(bundle);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    private void dismissProgress() {
        if (mProgress != null && mProgress.isShowing()) {
            try {
                mProgress.dismiss();
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            }
        }
    }
}
