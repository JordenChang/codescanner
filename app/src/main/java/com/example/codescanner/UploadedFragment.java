package com.example.codescanner;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.codescanner.image.DataImage;
import com.example.codescanner.image.ImageAdapter;
import com.example.codescanner.request.SendingRequestUtils;
import com.example.codescanner.request.UpdateImageUtils;
import com.example.codescanner.request.response.ImplResponseListener;
import com.example.codescanner.tools.EfficientBitmapHelper;
import com.example.codescanner.tools.PreferenceHelper;
import com.example.codescanner.tools.SizeHelper;
import com.example.codescanner.tools.StateCheck;
import com.example.codescanner.widget.RecyclerItemListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Mac on 15/4/14.
 */
public class UploadedFragment extends Fragment implements RecyclerItemListener.OnClickListener, StateCheck.StateListener {
    private static final String TAG = "uploaded fragment";
    private ViewStub mNoConnStub, mGridStub;
    private View mView;
    private ViewGroup mNoConnLayout;
    private ImageButton mRetryBtn;
    private boolean mIsNoConnInflated, mIsGridInflated;
    private ImplResponseListener mDataListener;
    private RecyclerView mImageGrid;
    private static Map<String, Integer> mHash = new HashMap<>();
    private static List<DataImage> mDataImages = new ArrayList<>();
    private boolean loading;
    private static int[] mSize;
    private UpdateImageUtils mUpdateUtils;
    private static ImageAdapter mImageAdapter;
    private GridLayoutManager mLayoutManager;
    private int mBottomPosition, mNewBottomPosition;
    private StateCheck mStateCheck;
    private AlertDialog mImageViewer;
    private Context mContext;
    private static boolean finish;
    private static Resources mResources;
    private static class ImageHandler extends Handler {

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            if (!finish) {
                Bundle bundle = msg.getData();
                boolean isSuccess = bundle.getBoolean("success");
                String hash = bundle.getString("hash");
                Log.d(TAG, hash+ ":" + isSuccess);
                int i = Math.abs(mHash.get(hash));
                if (isSuccess) {
                    byte[] data = bundle.getByteArray("data");
                    Bitmap bitmap = EfficientBitmapHelper.decodeSampleBitmap(data, mSize[0] / 5, mSize[1] / 5, hash);
                    Log.d(TAG, "set " + i);
                    mHash.put(hash, i);
                    mDataImages.get(i).setImage(bitmap);
                    mImageAdapter.setImage(i, bitmap);
                } else {
                    mHash.put(hash, -i);
                    Bitmap bitmap = BitmapFactory.decodeResource(mResources, R.drawable.ic_refresh);
                    mDataImages.get(i).setImage(bitmap);
                    mImageAdapter.setImage(i, bitmap);
                }
            }
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mSize = SizeHelper.getScreenSize(activity);
        mContext = activity;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mResources = getResources();
        mUpdateUtils = new UpdateImageUtils(mContext, new ImageHandler());
        mStateCheck = new StateCheck();
        mStateCheck.setStateListener(this);

        mDataListener = new ImplResponseListener(mContext) {
            @Override
            public void onFail() {
                loading = false;
                mStateCheck.setState(false);
                mUpdateUtils.setLoading(false);
            }

            @Override
            public void onSuccess(String result) {
                loading = false;
                mStateCheck.setState(true);
                mUpdateUtils.setLoading(false);
                if (result.contains("isEmpty")) {
                    Toast.makeText(mContext, "已無資料", Toast.LENGTH_SHORT).show();
                } else {
                    try {
                        JSONArray jArray = new JSONArray(result);
                        int size = jArray.length() + mBottomPosition;
                        if (size == 0) {
                            Toast.makeText(mContext, "尚無資料", Toast.LENGTH_SHORT).show();
                        }
                        for (int i = mBottomPosition; i < size; i++) {
                            int j = i - mBottomPosition;
                            JSONObject jResult = jArray.getJSONObject(j);
                            String hash = jResult.getString("hashcode");
                            String shop = jResult.getString("shopName");
                            int id = jResult.getInt("id");
                            Log.d(TAG, "shop: " + shop + "/" + hash);
                            DataImage di = new DataImage(id,
                                    hash,
                                    jResult.getString("authorName"),
                                    shop,
                                    jResult.getString("stockName"),
                                    jResult.getLong("postTime") * 1000);
                            mHash.put(hash, i);
                            mDataImages.add(di);
                            mUpdateUtils.update(hash);
                        }

                        if (!mIsGridInflated) {
                            initRecycler();
                        }

                        mUpdateUtils.start();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

            }
        };
/*

        });*/
        fetchData(0, 0);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_uploaded, container, false);
        mNoConnStub = (ViewStub) mView.findViewById(R.id.no_connect_stub);
        mGridStub = (ViewStub) mView.findViewById(R.id.grid_stub);

        return mView;
    }

    @Override
    public void onPause() {
        super.onPause();
        if(mImageViewer!=null && mImageViewer.isShowing()) {
            mImageViewer.dismiss();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        finish = true;
    }

    private void fetchData(int start, long since) {
        loading = true;
        SendingRequestUtils utils = new SendingRequestUtils("/album");
        Map<String, String> params = new HashMap<>();
        params.put("token", PreferenceHelper.getToken());
        params.put("mode", "3");
        params.put("start", String.valueOf(start));
        params.put("since", String.valueOf(since));
        utils.setUrlParams(params);

        utils.setResponseListener(mDataListener);
        utils.execute(SendingRequestUtils.HTTP_GET);
    }

    private void initRecycler() {
        mGridStub.inflate();
        mIsGridInflated = true;
        mImageGrid = (RecyclerView) mView.findViewById(R.id.grid_gallery);
        mLayoutManager = new GridLayoutManager(mContext, 3);
        mImageGrid.setLayoutManager(mLayoutManager);
        mImageAdapter = new ImageAdapter(mDataImages);
        mImageGrid.setAdapter(mImageAdapter);
        mImageGrid.addOnItemTouchListener(new RecyclerItemListener(mContext, this));
        mImageGrid.setOnScrollListener(new RecyclerView.OnScrollListener() {
            int lastVisibleItems, totalItemCount;

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                totalItemCount = mLayoutManager.getItemCount();
                lastVisibleItems = mLayoutManager.findLastCompletelyVisibleItemPosition();
                if ((lastVisibleItems + 1) % 10 == 0 || lastVisibleItems == totalItemCount) {
                    Log.d(TAG, "now pos: " + lastVisibleItems);
                    if (lastVisibleItems > mBottomPosition) {
                        Log.d(TAG, "pos: " + lastVisibleItems);

                        onScrollToBottom(lastVisibleItems + 1);
                    }

                }
            }
        });
    }

    private void onScrollToBottom(int pos) {
        if (!loading && !mUpdateUtils.isLoading()) {
            loading = true;
            mUpdateUtils.setLoading(loading);
            mNewBottomPosition = pos;
            DataImage dataImage = mDataImages.get(mDataImages.size() - 1);
            fetchData(dataImage.getId(), dataImage.getPostTime().getTime() / 1000);

        } else {
            Toast.makeText(mContext, "資料載入中...", Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void onClick(View v, int position) {


        String tag = (String) v.getTag();
        Log.d(TAG, "click: " + tag);
        if (tag.equals("empty")) {
            Toast.makeText(mContext, "圖片載入中...", Toast.LENGTH_SHORT).show();
        } else {
            if (mHash.get(tag) < 0) {
                Log.d(TAG, "fail image: " + tag);
                mUpdateUtils.update(tag);
                mUpdateUtils.start();
            } else {
                final int[] dimen = EfficientBitmapHelper.optionsMap.get(tag);
                Log.d(TAG, "zoom" + Arrays.toString(dimen));
                ImageView image = (ImageView) v.findViewById(R.id.content_img);
                new AsyncTask<Bitmap, Void, Bitmap>() {
                    @Override
                    protected Bitmap doInBackground(Bitmap... params) {
                        return Bitmap.createScaledBitmap(params[0], dimen[0], dimen[1], false);
                    }

                    @Override
                    protected void onPostExecute(Bitmap bitmap) {
                        super.onPostExecute(bitmap);

                        ImageView showImg = new ImageView(mContext);
                        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                                ViewGroup.LayoutParams.MATCH_PARENT,
                                ViewGroup.LayoutParams.MATCH_PARENT);
                        showImg.setLayoutParams(lp);

                        showImg.setScaleType(ImageView.ScaleType.FIT_CENTER);
                        showImg.setImageBitmap(bitmap);

                        mImageViewer = new AlertDialog.Builder(mContext).setView(showImg).setOnCancelListener(new DialogInterface.OnCancelListener() {
                            @Override
                            public void onCancel(DialogInterface dialog) {
                                System.gc();
                            }
                        }).create();
                        mImageViewer.show();

                    }
                }.execute(((BitmapDrawable) image.getDrawable()).getBitmap());

            }
        }
    }

    @Override
    public void onFail() {
        if (!mIsNoConnInflated) {
            Log.d(TAG, "inflate");
            mNoConnStub.inflate();
            mIsNoConnInflated = true;
            mNoConnLayout = (ViewGroup) mView.findViewById(R.id.no_connect_layout);
            mRetryBtn = (ImageButton) mView.findViewById(R.id.retry_btn);
            mRetryBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    fetchData(0, 0);
                }
            });
        }
    }

    @Override
    public void onLoad() {
        if (mNoConnLayout != null) {
            mNoConnLayout.setVisibility(View.GONE);
        }

        mBottomPosition = mNewBottomPosition;
        System.gc();
    }

    @Override
    public void onPartialFail() {
        Toast.makeText(mContext, "無法連線，請再試一次", Toast.LENGTH_SHORT).show();
    }
}
