package com.example.codescanner;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.codescanner.board.BoardListActivity;
import com.example.codescanner.request.LoginUtil;
import com.example.codescanner.request.SendingRequestUtils;
import com.example.codescanner.request.response.ResponseListener;
import com.example.codescanner.request.response.SimpleResponseListener;
import com.example.codescanner.tools.PreferenceHelper;
import com.testin.agent.TestinAgent;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Mac on 15/4/13.
 */
public class LoginActivity extends Activity implements ResponseListener {
    private static final String TAG = "login activity";
    private EditText mIdText, mPwdText;
    private LoginUtil mLoginUtils;
    private String mId, mPwd;
    private boolean mHasLogged;
    private ProgressDialog mProgressDialog;
    private SharedPreferences mUserInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        TestinAgent.init(this, "e718a3bc97ee545ac673b247761546c5", "debug");
        setContentView(R.layout.activity_login);

        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setMessage("登入中...");

        mIdText = (EditText) findViewById(R.id.id_text);
        mPwdText = (EditText) findViewById(R.id.pwd_text);

        mLoginUtils = new LoginUtil(this);
        mLoginUtils.setLoginListener(this);

        PreferenceHelper.init(this);
        mUserInfo = PreferenceHelper.getUserInfo();
        mId = mUserInfo.getString("id", "");
        mPwd = mUserInfo.getString("password", "");
        mHasLogged = mUserInfo.getBoolean("isLogin", false);
        Log.d(TAG, "get info : " + mId + "/" + mPwd + "is login: " + mHasLogged);
        if (!mId.isEmpty() && !mPwd.isEmpty() && mHasLogged && !getIntent().getBooleanExtra("logout", false)) {
            mProgressDialog.show();

            mIdText.setText(mId);
            mPwdText.setText(mPwd);
            mLoginUtils.login(mId, mPwd);
        }
    }

    @Override
    protected void onDestroy() {
        dismissProgress();
        super.onDestroy();
    }

    public void login(View v) {

        mId = mIdText.getText().toString();
        mPwd = mPwdText.getText().toString();

        if (mId.isEmpty() || mPwd.isEmpty()) {
            Toast.makeText(this, "帳號或密碼不能為空", Toast.LENGTH_SHORT).show();
        } else {
            if (mProgressDialog != null && !mProgressDialog.isShowing()) {
                mProgressDialog.show();
            }
            mLoginUtils.login(mId, mPwd);
        }
    }

    @Override
    public void onSuccess(String result) {

        mIdText.setText("");
        mPwdText.setText("");
        String token = "";
        try {
            JSONObject jResult = new JSONObject(result);
            token = jResult.optString("token");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        // Gets the branch


        // Stored in preference
        SendingRequestUtils utils = new SendingRequestUtils("/users/" + mId);
        utils.setUrlParam("token", token);
        utils.setResponseListener(new SimpleResponseListener() {
            @Override
            public void onSuccess(String result) {
                try {
                    JSONObject jResult = new JSONObject(result);

                    mUserInfo.edit()
                            .putString("user_name", jResult.getString("name"))
                            .putString("branches", jResult.optString("shop").replaceAll("[\"\\]\\[\\\\]", "").replaceAll(",", "\n"))
                            .apply();
                    startToMain();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });
        utils.execute(SendingRequestUtils.HTTP_GET);
//        if(!mHasLogged) {
        Log.d(TAG, "store:" + mId + "/" + mPwd);
        mUserInfo.edit().putString("id", mId).putString("password", mPwd).putBoolean("isLogin", true).apply();
//        }

    }

    @Override
    public void onClientError(String errorMsg) {
        dismissProgress();
        Toast.makeText(this, "帳號或密碼錯誤", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onServerError(String errorMsg) {
        dismissProgress();
        Toast.makeText(this, "登入失敗，請稍候再試", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onNoResponse() {
        dismissProgress();
        Toast.makeText(this, "登入失敗，請稍候再試", Toast.LENGTH_SHORT).show();
    }

    public void startToMain() {
        dismissProgress();
        Intent intent = new Intent(this, BoardListActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    private void  dismissProgress() {
        try {
            if (mProgressDialog != null && mProgressDialog.isShowing()) {
                mProgressDialog.dismiss();
            }
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
    }
}
