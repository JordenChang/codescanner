package com.example.codescanner;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.codescanner.board.BoardListActivity;
import com.example.codescanner.setting.SettingActivity;
import com.example.codescanner.tools.PreferenceHelper;
import com.example.codescanner.tools.RelaunchFlag;
import com.example.codescanner.widget.QueryWindow;
import com.testin.agent.TestinAgent;

/**
 * Created by Mac on 15/4/13.
 */
public class MainActivity extends ActionBarActivity implements View.OnClickListener {
    public static final int REQUEST_CODE = 0x0000c0de;
    private static final String TAG = "main";
    private QueryWindow mQueryWindow;
    private static long back_pressed;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        mQueryWindow = new QueryWindow(this);
        TextView mUserInfoText = (TextView) findViewById(R.id.user_info);
        PreferenceHelper.init(this);
        SharedPreferences pref = PreferenceHelper.getUserInfo();
        String id = pref.getString("id", "");
        TestinAgent.setUserInfo(id);
        mUserInfoText.setText(pref.getString("user_name", "") + "@" + id);


    }

    @Override
    protected void onPause() {
        super.onPause();
        if(mQueryWindow!=null) {
            mQueryWindow.dismiss();
        }
        RelaunchFlag.getInstance().setIsNeed(true);
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        if(RelaunchFlag.getInstance().isNeed()) {
            Intent intent = new Intent(this, BoardListActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_setting:
                startActivity(SettingActivity.class);
                break;
            case R.id.board:
                startActivity(BoardListActivity.class);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.upload_btn:
                startActivity(UploadActivity.class);
                break;
            case R.id.check_in_btn:
                startActivity(CheckInActivity.class);
                break;
            case R.id.search_btn:
                mQueryWindow.showQueryWindow("");
                break;
            case R.id.uploaded_btn:
                startActivity(UploadedActivity.class);
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d(TAG, requestCode+"/result: " + resultCode);
        if (resultCode == RESULT_OK) {
            String barCode = data.getStringExtra("SCAN_RESULT");
            Log.d(TAG, "barcode: " + barCode);
            mQueryWindow.showQueryWindow(barCode);
        }
        super.onActivityResult(requestCode, resultCode, data);
        RelaunchFlag.getInstance().setIsNeed(false);
    }

    private void startActivity(Class clazz) {
        Intent intent = new Intent(this, clazz);
        startActivity(intent);
    }

    public void startScan() {
        Intent scanIntent = new Intent("com.google.zxing.client.android.SCAN");
        startActivityForResult(scanIntent, REQUEST_CODE);
    }

    @Override
    public void onBackPressed() {
        Log.d(TAG, "back press");

        if (back_pressed + 2000 > System.currentTimeMillis()) {
            finish();
        } else {
            Toast.makeText(getBaseContext(), "再按一次返回鍵退出", Toast.LENGTH_SHORT).show();
        }
        back_pressed = System.currentTimeMillis();
    }
}
