package com.example.codescanner.image;

import android.graphics.Bitmap;

import java.util.Date;

/**
 * Created by Mac on 15/5/10.
 */
public class DataImage {
    private String mHash, mAuthor, mShop, mBranch;
    private Date mPostTime;
    private Bitmap mImage;
    private int mId;

    public DataImage(int id, String hash, String author, String shop, String branch, long time) {
        mId = id;
        this.mHash = hash;
        mBranch = branch;
        this.mAuthor = author;
        this.mShop = shop;
        mPostTime = new Date(time);
        
    }

    public int getId() {
        return mId;
    }

    public String getHash() {
        return mHash;
    }

    public String getAuthor() {
        return mAuthor;
    }

    public String getShop() {
        return mShop;
    }

    public Date getPostTime() {
        return mPostTime;
    }

    public void setImage(Bitmap image) {
        mImage = image;
    }
    public Bitmap getImage() {
        return mImage;
    }

    public String getBranch() {
        return mBranch;
    }
}
