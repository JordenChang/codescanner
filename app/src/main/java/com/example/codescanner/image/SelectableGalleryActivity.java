package com.example.codescanner.image;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.codescanner.R;
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.cache.memory.impl.WeakMemoryCache;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import java.util.ArrayList;
import java.util.Collections;

public class SelectableGalleryActivity extends Activity {

    private static final String TAG = "gallery";
    GridView gridGallery;
    Handler handler;
    SelectableGalleryAdapter adapter;

    ImageView imgNoMedia;
    Button btnGalleryOk;
    Button btnGalleryCancel;
    TextView galleryTitle;

    String action;
    String okButtonText = null;
    String cancelButtonText = null;
    String titleText = null;
    String errorMessageText = null;
    Integer limit = 5;
    private ImageLoader imageLoader;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        //setContentView(R.layout.gallery);
        setContentView(R.layout.gallery);

        action = getIntent().getAction();
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            cancelButtonText = extras.getString("cancelButtonText");
            okButtonText = extras.getString("okButtonText");
            titleText = extras.getString("titleText");
            errorMessageText = extras.getString("errorMessageText");
            limit = extras.getInt("limit", 5);
        }

        if (action == null) {
            finish();
        }
        initImageLoader();
        init();
    }

    private void initImageLoader() {
        ImageLoaderConfiguration.Builder builder = new ImageLoaderConfiguration.Builder(this)
                .threadPoolSize(3)
                .threadPriority(Thread.NORM_PRIORITY - 2)
                .memoryCacheSize(1500000) // 1.5 Mb
                .denyCacheImageMultipleSizesInMemory()
                .memoryCache(new WeakMemoryCache())
                .discCacheFileNameGenerator(new Md5FileNameGenerator());

        ImageLoaderConfiguration config = builder.build();
        imageLoader = ImageLoader.getInstance();
        imageLoader.init(config);
        imageLoader.clearDiscCache();
        imageLoader.clearMemoryCache();

    }

    private void init() {

        handler = new Handler();
        gridGallery = (GridView) findViewById(R.id.gridGallery);
        gridGallery.setFastScrollEnabled(true);
        adapter = new SelectableGalleryAdapter(getApplicationContext(), imageLoader, errorMessageText);
        Log.d(TAG, "action: " + action);
        if (action.equalsIgnoreCase(Action.ACTION_MULTIPLE_PICK)) {

            findViewById(R.id.llBottomContainer).setVisibility(View.VISIBLE);
            gridGallery.setOnItemClickListener(mItemMulClickListener);
            adapter.setMultiplePick(true);

        } else if (action.equalsIgnoreCase(Action.ACTION_PICK)) {

            findViewById(R.id.llBottomContainer).setVisibility(View.GONE);
            gridGallery.setOnItemClickListener(mItemSingleClickListener);
            adapter.setMultiplePick(false);

        }

        gridGallery.setAdapter(adapter);
        imgNoMedia = (ImageView) findViewById(R.id.imgNoMedia);

        btnGalleryOk = (Button) findViewById(R.id.btnGalleryOk);
        btnGalleryCancel = (Button) findViewById(R.id.btnGalleryCancel);
        galleryTitle = (TextView) findViewById(R.id.tvTitleText);

        if (titleText != null) {
            galleryTitle.setText(titleText);
        }

        if (okButtonText != null) {
            btnGalleryOk.setText(okButtonText);
        }

        if (cancelButtonText != null) {
            btnGalleryCancel.setText(cancelButtonText);
        }

        btnGalleryCancel.setOnClickListener(mCancelClickListener);
        btnGalleryOk.setOnClickListener(mOkClickListener);

        new Thread() {

            @Override
            public void run() {
                Looper.prepare();
                handler.post(new Runnable() {

                    @Override
                    public void run() {
                        adapter.addAll(getGalleryPhotos());
                        checkImageStatus();
                    }
                });
                Looper.loop();
            }

            ;

        }.start();

    }

    private void checkImageStatus() {
        if (adapter.isEmpty()) {
            imgNoMedia.setVisibility(View.VISIBLE);
        } else {
            imgNoMedia.setVisibility(View.GONE);
        }
    }

    View.OnClickListener mOkClickListener = new View.OnClickListener() {

        @Override
        public void onClick(View v) {

            ArrayList<SelectableGallery> selected = adapter.getSelected();
            if(selected.size()>0) {
                String[] allPath = new String[selected.size()];
                for (int i = 0; i < allPath.length; i++) {
                    allPath[i] = selected.get(i).sdcardPath;
                }

                //clear adapter
                adapter.clear();
                Intent data = new Intent().putExtra("all_path", allPath);
                try {
                    setResult(RESULT_OK, data);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                finish();
            } else {
                adapter.clear();
                adapter.clearCache();
                setResult(RESULT_CANCELED);
                finish();
            }

        }
    };

    View.OnClickListener mCancelClickListener = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            //clear adapter
            adapter.clear();
            adapter.clearCache();
            setResult(RESULT_CANCELED);
            finish();
        }
    };

    AdapterView.OnItemClickListener mItemMulClickListener = new AdapterView.OnItemClickListener() {

        @Override
        public void onItemClick(AdapterView<?> l, View v, int position, long id) {
            adapter.changeSelection(v, position, limit);

        }
    };

    AdapterView.OnItemClickListener mItemSingleClickListener = new AdapterView.OnItemClickListener() {

        @Override
        public void onItemClick(AdapterView<?> l, View v, int position, long id) {
            SelectableGallery item = adapter.getItem(position);
            Intent data = new Intent().putExtra("single_path", item.sdcardPath);
            setResult(RESULT_OK, data);
            finish();
        }
    };

    private ArrayList<SelectableGallery> getGalleryPhotos() {
        ArrayList<SelectableGallery> galleryList = new ArrayList<SelectableGallery>();

        final String[] columns = {MediaStore.Images.Media.DATA,
                MediaStore.Images.Media._ID, MediaStore.Images.Thumbnails.DATA};
        final String orderBy = MediaStore.Images.Media._ID;

        @SuppressWarnings("deprecation")
        Cursor imagecursor = managedQuery(
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI, columns,
                null, null, orderBy);
        if (imagecursor != null && imagecursor.getCount() > 0) {

            while (imagecursor.moveToNext()) {
                SelectableGallery item = new SelectableGallery();

                int dataColumnIndex = imagecursor
                        .getColumnIndex(MediaStore.Images.Media.DATA);
                int thumbColumnIndex = imagecursor.getColumnIndex(MediaStore.Images.Thumbnails.DATA);
                String path = imagecursor.getString(dataColumnIndex);
                Log.d(TAG, "file path: " + path);
                item.sdcardPath = path;
                galleryList.add(item);
            }
        }


        // show newest photo at beginning of the list
        Collections.reverse(galleryList);
        return galleryList;
    }

    @Override
    public void onBackPressed() {
        adapter.clear();
        adapter.clearCache();
        setResult(RESULT_CANCELED);
        super.onBackPressed();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (imageLoader != null) {
            imageLoader.stop();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (imageLoader != null) {
            imageLoader.destroy();
        }
    }
}
