package com.example.codescanner.image;

import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.codescanner.R;

import java.text.SimpleDateFormat;
import java.util.List;

/**
 * Created by Mac on 15/5/10.
 */
public class ImageAdapter extends RecyclerView.Adapter<ImageAdapter.ImageHolder> {
    private static final String TAG = "image adapter";
    private static List<DataImage> mImages;

    public ImageAdapter(List<DataImage> images) {
        mImages = images;
    }

    @Override
    public ImageHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = View.inflate(parent.getContext(), R.layout.layout_gallery_item, null);
        v.setTag("empty");
        return new ImageHolder(v);
    }

    @Override
    public void onBindViewHolder(ImageHolder holder, int position) {
        DataImage dataImage = mImages.get(position);
        Bitmap bitmap = dataImage.getImage();
        if (bitmap != null) {
            holder.view.setTag(dataImage.getHash());
            String shop = dataImage.getShop();
            Log.d(TAG, "shop: " + shop);
            shop = shop.replaceAll("[0-9//]", "");
//            shop = shop.split("/")[1];
            String branch = dataImage.getBranch();
//            branch = branch.split("/")[1];
            Log.d(TAG, "bind: " + position + "/" + shop);
            holder.imageView.setImageBitmap(bitmap);
            SimpleDateFormat format = new SimpleDateFormat("MM-dd hh:mm");
            String date = format.format(dataImage.getPostTime());
            holder.shopText.setText(shop);
            holder.dateText.setText(date);
            holder.branchText.setText(branch);
        }
    }

    @Override
    public int getItemCount() {
        return mImages.size();
    }

    public static class ImageHolder extends RecyclerView.ViewHolder {
        ImageView imageView;
        TextView dateText, shopText, branchText;
        View view;
        public ImageHolder(View itemView) {
            super(itemView);
            view = itemView;
            imageView = (ImageView) itemView.findViewById(R.id.content_img);
            dateText = (TextView) itemView.findViewById(R.id.date_text);
            shopText = (TextView) itemView.findViewById(R.id.shop_text);
            branchText = (TextView) itemView.findViewById(R.id.branch_text);
        }
    }

    public void setImage(int i, Bitmap bitmap) {
        mImages.get(i).setImage(bitmap);
        notifyItemChanged(i);
    }



}
