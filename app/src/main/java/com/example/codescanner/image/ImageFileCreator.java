package com.example.codescanner.image;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Mac on 15/5/5.
 */
public class ImageFileCreator {

    private static final String TAG = "image creator";

    public static Uri getOutputMediaFileUri() {

        return Uri.fromFile(getOutputMediaFile("IMG"));
    }

    public static File getOutputMediaFile(String type) {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmssSSSS").format(new Date());
        Log.d(TAG, "create file: " + timeStamp);
        File store = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File internal = new File(store, "連興行銷");
        if (!internal.exists()) {
            Log.d(TAG, "not exist, create one");
            internal.mkdir();
        }

        return new File(internal, type + "_" + timeStamp + ".jpg");
    }

    public static File compressBitmap(String path) {
        Bitmap bitmap = BitmapFactory.decodeFile(path);
        Matrix offset = offsetRotation(path);
        if (offset != null) {
            bitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), offset, true);
        }
        OutputStream out;
        if (bitmap != null) {
            try {
                File file = getOutputMediaFile("CMP");
                Log.d(TAG, "created file: " + file);
                out = new FileOutputStream(file);
                bitmap.compress(Bitmap.CompressFormat.JPEG, 20, out);
                out.flush();
                Log.d(TAG, file.toString());
                out.close();
                return file;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    /**
     * Get offset the degree if necessary
     * @param path of image
     * @return offset degree matrix
     */
    private static Matrix offsetRotation(String path) {
        try {
            Matrix matrix = new Matrix();
            ExifInterface ei = new ExifInterface(path);
            int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
            Log.d(TAG, "degree: " + orientation);

            switch (orientation) {
                case ExifInterface.ORIENTATION_ROTATE_90:
                    matrix.postRotate(90);
                    return matrix;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    matrix.postRotate(180);
                    return matrix;
                case ExifInterface.ORIENTATION_ROTATE_270:
                    matrix.postRotate(270);
                    return matrix;

            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;

    }


}
