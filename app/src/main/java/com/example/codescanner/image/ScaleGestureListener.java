package com.example.codescanner.image;

import android.view.ScaleGestureDetector;
import android.view.ScaleGestureDetector.OnScaleGestureListener;
import android.view.View;

public class ScaleGestureListener implements OnScaleGestureListener {
    private View mView;
    private float mScale = 0f;

    public ScaleGestureListener(View view) {
        this.mView = view;
    }

    public View getView() {
        return mView;
    }

    public void setView(View mView) {
        this.mView = mView;
    }

    @Override
    public boolean onScaleBegin(ScaleGestureDetector detector) {
        // TODO Auto-generated method stub
        if (mScale == 0f) {
            mScale = detector.getScaleFactor();
        }


        return true;
    }

    @Override
    public boolean onScale(ScaleGestureDetector detector) {
        // TODO Auto-generated method stub

        mScale = detector.getScaleFactor() + (mScale - 1);

        if (mScale >= 3.0f) {
            mScale = 3.0f;
        } else if(mScale<=1.0f){
            mScale = 1.0f;
        }



        mView.setScaleX(mScale);
        mView.setScaleY(mScale);
        return false;
    }

    @Override
    public void onScaleEnd(ScaleGestureDetector detector) {
        // TODO Auto-generated method stub

    }

}
