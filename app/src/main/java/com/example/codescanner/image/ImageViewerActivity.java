package com.example.codescanner.image;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ZoomControls;

import com.example.codescanner.R;

/**
 * Created by Mac on 15/6/4.
 */
public class ImageViewerActivity extends ActionBarActivity {
    private static String TAG = "image viewer";
    private ImageView mContentImg;

//    private ScaleGestureListener mScaleListener;
//    private ScaleGestureDetector mScaleDetector;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activitty_image_viewer);


        Intent intent = getIntent();
        if (intent != null) {

            mContentImg = (ImageView) findViewById(R.id.content_img);
            ZoomControls zoom = (ZoomControls) findViewById(R.id.zoom);
            zoom.setOnZoomInClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    float x = mContentImg.getScaleX();
                    float y = mContentImg.getScaleY();
                    if(x<3 && y < 3) {
                        mContentImg.setScaleX(x+0.5f);
                        mContentImg.setScaleY(y+0.5f);
                    }
                }
            });
            zoom.setOnZoomOutClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    float x = mContentImg.getScaleX();
                    float y = mContentImg.getScaleY();
                    if(x>1 && y > 1) {
                        mContentImg.setScaleX(x-0.5f);
                        mContentImg.setScaleY(y-0.5f);
                    }
                }
            });
            mContentImg.setOnTouchListener(new View.OnTouchListener() {
                int prevX, prevY;
                boolean deJump;
                RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);

                @Override
                public boolean onTouch(View v, MotionEvent event) {
//                    mScaleDetector.onTouchEvent(event);

                    switch (event.getAction()) {
                        case MotionEvent.ACTION_DOWN:
                            deJump = false;
                            prevX = (int) event.getRawX();
                            prevY = (int) event.getRawY();
                            lp.bottomMargin = -2 * v.getHeight();
                            lp.rightMargin = -2 * v.getWidth();
                            v.setLayoutParams(lp);
                            return true;
                        case MotionEvent.ACTION_MOVE:
                            if (!deJump) {
                                lp.topMargin += (int) event.getRawY() - prevY;
                                prevY = (int) event.getRawY();
                                lp.leftMargin += (int) event.getRawX() - prevX;
                                prevX = (int) event.getRawX();
                                v.setLayoutParams(lp);
                            }

                            return true;
                        case MotionEvent.ACTION_POINTER_UP:
                            deJump = true;
                            return true;
                        case MotionEvent.ACTION_POINTER_DOWN:
                            deJump = true;
                            return true;
                        default:
                            return false;
                    }

                }


            });
//            mScaleListener = new ScaleGestureListener(mContentImg);
//            mScaleDetector = new ScaleGestureDetector(this, mScaleListener);
            new AsyncTask<Intent, Void, Bitmap>() {
                @Override
                protected Bitmap doInBackground(Intent... params) {
                    byte[] byteArray = params[0].getByteArrayExtra("image");
                    int[] dimen = params[0].getIntArrayExtra("dimen");
                    Bitmap bitmap = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
                    return Bitmap.createScaledBitmap(bitmap, dimen[0], dimen[1], false);
                }

                @Override
                protected void onPostExecute(Bitmap bitmap) {
                    super.onPostExecute(bitmap);
                    mContentImg.setImageBitmap(bitmap);

                }
            }.execute(intent);
        } else

        {
            finish();
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ((BitmapDrawable)mContentImg.getDrawable()).getBitmap().recycle();
        mContentImg = null;
        System.gc();
    }
}

