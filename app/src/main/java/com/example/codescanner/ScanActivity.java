package com.example.codescanner;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.app.ActionBarActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by Mac on 15/4/14.
 */
public class ScanActivity extends ActionBarActivity {
    private static final String TAG = "scan activity";
    public static final int REQUEST_CODE = 0x0000c0de;
    private TextView typeText, resultText;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan);
        typeText = (TextView) findViewById(R.id.type_text);
        resultText = (TextView) findViewById(R.id.result_text);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }
    public void scan(View v) {
        Intent scanIntent = new Intent("com.google.zxing.client.android.SCAN");
        startActivityForResult(scanIntent, REQUEST_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(resultCode==RESULT_OK) {
            Toast.makeText(this, "code: " + data.getStringExtra("SCAN_RESULT"), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if(id==android.R.id.home) {
            NavUtils.navigateUpFromSameTask(this);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


}
