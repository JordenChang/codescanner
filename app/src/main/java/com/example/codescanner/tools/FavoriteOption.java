package com.example.codescanner.tools;

import android.util.Log;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

/**
 * Created by Mac on 15/5/3.
 */
public class FavoriteOption implements Comparable<FavoriteOption> {
    private static final String TAG = "Favorite Option";

    @Override
    public int compareTo(FavoriteOption another) {
        if (isFavorite == another.isFavorite) {
            return name.equals(another.name) ? 0 : (favoritePoint > another.favoritePoint ? -1 : 1);
        }
        return name.equals(another.name) ? 0 : isFavorite ? -1 : 1;
    }

    public static class FavoriteComparator implements Comparator<FavoriteOption> {
        @Override
        public int compare(FavoriteOption lhs, FavoriteOption rhs) {
            if (lhs.isFavorite == rhs.isFavorite) {
                int l = lhs.favoritePoint;
                int r = rhs.favoritePoint;
                return lhs.name.equals(rhs.name) ? 0 : (l > r ? -1 : 1);
            }
            return lhs.name.equals(rhs.name) ? 0 : lhs.isFavorite ? -1 : 1;
        }

    }

    private String name;
    private boolean isFavorite;
    private int favoritePoint;
    private static FavoriteComparator comparator = new FavoriteComparator();

    public FavoriteOption(String name, int favoritePoint, boolean isFavorite) {
        this.name = name;
        this.isFavorite = isFavorite;
        this.favoritePoint = favoritePoint;

    }

    public void setFavorite(boolean isFavorite) {
        this.isFavorite = isFavorite;
    }

    public String getName() {
        return name;
    }

    public boolean isFavorite() {
        return isFavorite;
    }

    public int getFavoritePoint() {
        return favoritePoint;
    }

    public void setFavoritePoint(int favoritePoint) {
        this.favoritePoint = favoritePoint;
    }

    public static List<FavoriteOption> sort(List<FavoriteOption> options) {
        FavoriteOption[] optionArray = new FavoriteOption[options.size()];
        optionArray = options.toArray(optionArray);
        Log.d(TAG, "sorted[" + optionArray + "]");
        return sort(optionArray);
    }

    public static List<FavoriteOption> sort(FavoriteOption[] options) {
        for (int i = 1; i < options.length; i++) {
            FavoriteOption option = options[i];
            Log.d(TAG, "option: " + option);
            int j = i - 1;
            while (j >= 0 && comparator.compare(option, options[j]) < 0) {
                Log.d(TAG, "swap: " + options[j] + "/" + options[j + 1]);
                options[j + 1] = options[j];
                j--;
            }
            options[j + 1] = option;
        }
        return new ArrayList<>(Arrays.asList(options));
    }

    @Override
    public boolean equals(Object o) {
        return o instanceof FavoriteOption && name.equals(((FavoriteOption) o).name);
    }

    @Override
    public String toString() {
        return name;
    }
}
