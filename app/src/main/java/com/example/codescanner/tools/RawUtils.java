package com.example.codescanner.tools;

import android.content.Context;

import com.example.codescanner.R;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created by Mac on 15/4/21.
 */
public class RawUtils {
    private static final String TAG = "branch util";
    private static ArrayList<String> mBranches = new ArrayList<>();
    private static Context mContext;

    public static ArrayList<String> getBranches(Context context) {
        if (mContext == null) {
            mContext = context;
        }
        if (mBranches.size() > 0) {
            return mBranches;
        }

        Scanner scanner = new Scanner(mContext.getResources().openRawResource(R.raw.branch_name), "big-5");

        while (scanner.hasNext()) {
            String s = scanner.next();
            s = s.trim();
            s = s.replaceAll("[',]", "");
            mBranches.add(s);
        }
        scanner.close();
        return mBranches;


    }
}
