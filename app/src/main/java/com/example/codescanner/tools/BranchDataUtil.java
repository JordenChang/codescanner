package com.example.codescanner.tools;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeSet;

/**
 * Created by Mac on 15/4/21.
 */
public class BranchDataUtil implements FavoriteDataUtil {
    private static final String TAG = "branch";
    private static Set<FavoriteOption> mBranches = new TreeSet<FavoriteOption>(new FavoriteOption.FavoriteComparator()) {
        @Override
        public boolean contains(Object object) {
            return super.contains(object) && equals(object);
        }
    };
    private static List<Observer> mObservers = new ArrayList<>();
    private static SharedPreferences mFavoritePref;
    private static SharedPreferences.Editor mFavoriteEditor;
    private static boolean isInit;
    public BranchDataUtil(Context context) {
        if(!isInit) {
            // gets the preference for favorite branch
            if (mFavoritePref == null || mFavoriteEditor == null) {
                PreferenceHelper.init(context);
                mFavoritePref = PreferenceHelper.getBranchInfo();
                mFavoriteEditor = mFavoritePref.edit();
            }

            Scanner scanner = new Scanner(PreferenceHelper.getUserInfo().getString("branches", ""));
            while (scanner.hasNext()) {
                String name = scanner.nextLine();
                name = name.trim();
                // instantiate with preference
                boolean success = mBranches.add(new FavoriteOption(name,
                        mFavoritePref.getInt("i_" + name, 0),
                        mFavoritePref.getBoolean("b_" + name, false)));
            }
            isInit = true;
        }
    }

    /**
     * If branches is not sorted with favorite point, sort it and return.
     *
     * @return sorted branches array
     */
    @Override
    public FavoriteOption[] getOptions() {

        return mBranches.toArray(new FavoriteOption[mBranches.size()]);
    }

    /**
     * Higher the favorite point of certain branch.
     *
     * @param option favorite branch
     */
    public void addFavoritePoint(FavoriteOption option) {
//        int fav = mFavoritePref.getInt(option.getName(), 0) + 1;
//        mFavoriteEditor.putInt("i_" + option.getName(), fav).apply();
//        for (FavoriteOption o : mBranches) {
//            if (o.equals(option)) {
//                o.setFavoritePoint(fav);
//                break;
//            }
//        }
        notifyObserver();
    }

    public void addFavorite(FavoriteOption option) {
//        Log.d(TAG, "fav: " + option.getName());
//        mFavoriteEditor.putBoolean("b_" + option.getName(), true).apply();
//        for (FavoriteOption o : mBranches) {
//            if (o.equals(option)) {
//                o.setFavorite(true);
//                break;
//            }
//        }
        notifyObserver();
    }

    public void removeFavorite(FavoriteOption option) {
        mFavoriteEditor.putBoolean("b_" + option.getName(), false).apply();
        for (FavoriteOption o : mBranches) {
            if (o.equals(option)) {
                o.setFavorite(false);
                break;
            }
        }
        notifyObserver();
    }

    public void initStatus() {
        Log.d(TAG, "init");
        isInit = false;
        mFavoriteEditor.clear().apply();
        for (FavoriteOption o : mBranches) {
            o.setFavorite(false);
            o.setFavoritePoint(0);
        }
        notifyObserver();
    }


    /**
     * Add observer.
     *
     * @param observer is new observer to observe branch data
     */
    @Override
    public void attach(Observer observer) {
        mObservers.add(observer);
    }

    /**
     * Remove observer.
     *
     * @param observer is not interested in this data any more.
     */
    @Override
    public void detach(Observer observer) {
        mObservers.remove(observer);
    }

    /**
     * Notifies all observer that data was changed.
     */
    @Override
    public void notifyObserver() {
//        mBranches = new TreeSet<>(mBranches);
//        int amount = mObservers.size();
//        for (int i = 0; i < amount; i++) {
//            mObservers.get(i).update(getOptions());
//        }
    }

}
