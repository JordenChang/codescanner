package com.example.codescanner.tools;

import android.util.Log;

/**
 * Created by Jorden on 2015/11/25.
 */
public class RelaunchFlag {
    private static final String TAG = RelaunchFlag.class.getSimpleName();
    private static RelaunchFlag ourInstance = new RelaunchFlag();
    private boolean isNeed;
    public static RelaunchFlag getInstance() {
        return ourInstance;
    }

    private RelaunchFlag() {
    }

    public static RelaunchFlag getOurInstance() {
        return ourInstance;
    }

    public static void setOurInstance(RelaunchFlag ourInstance) {

        RelaunchFlag.ourInstance = ourInstance;
    }

    public boolean isNeed() {
        return isNeed;
    }

    public void setIsNeed(boolean isNeed) {
        Log.d(TAG, "set as " + isNeed);
        this.isNeed = isNeed;
    }
}