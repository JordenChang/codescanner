package com.example.codescanner.tools;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.graphics.Point;
import android.view.Display;

/**
 * Created by Mac on 15/5/10.
 */
public class SizeHelper {
    @SuppressLint("NewApi")
    public static int[] getScreenSize(Activity context) {
        Display display = context.getWindowManager().getDefaultDisplay();
        int[] sizes;
        if (APIComparer.isGreaterOrEqual(13)) {
            Point size = new Point();
            display.getSize(size);
            sizes = new int[]{size.x, size.y};
        } else {
            sizes = new int[]{display.getHeight(), display.getWidth()};
        }
        return sizes;
    }
}
