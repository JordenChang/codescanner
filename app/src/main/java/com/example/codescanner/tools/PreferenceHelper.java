package com.example.codescanner.tools;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Mac on 15/5/1.
 */
public class PreferenceHelper {
    private static Context mContext;
    private static SharedPreferences mUserInfo;
    private static SharedPreferences mBranchInfo;
    private static SharedPreferences mManInfo;
    public static boolean mInit;

    public static void init(Context context) {
        mContext = context;
        mUserInfo = mContext.getSharedPreferences("user_info", Context.MODE_PRIVATE);
        mBranchInfo = mContext.getSharedPreferences(getId()+"branch_favorite", Context.MODE_PRIVATE);
        mManInfo = mContext.getSharedPreferences("man_favorite", Context.MODE_PRIVATE);
        mInit = true;
    }

    public static SharedPreferences getUserInfo() {
        if (mInit) {
            return mUserInfo;
        } else {
            throw new IllegalStateException("Calls init() first");
        }
    }

    public static SharedPreferences getBranchInfo() {
        if (mInit) {
            return mBranchInfo;
        } else {
            throw new IllegalStateException("Calls init() first");
        }
    }

    public static SharedPreferences getManInfo() {
        if (mInit) {
            return mBranchInfo;
        } else {
            throw new IllegalStateException("Calls init() first");
        }
    }
    public static SharedPreferences getReadInfo() {
        if (mInit) {
            return mContext.getSharedPreferences(mUserInfo.getString("id", "")+"_read", Context.MODE_PRIVATE);
        } else {
            throw new IllegalStateException("Calls init() first");
        }
    }

    public static String getToken() {
        return mUserInfo.getString("token", "");
    }
    public static String getId() {return mUserInfo.getString("id", "");}

    public static boolean getLastMonth() {
        return mUserInfo.getBoolean("last_month", false);
    }

}
