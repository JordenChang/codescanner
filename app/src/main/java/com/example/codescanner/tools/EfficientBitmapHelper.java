package com.example.codescanner.tools;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Mac on 15/3/8.
 */
public class EfficientBitmapHelper {
    private static final String TAG = "bitmap";
    public static Map<String, int[]> optionsMap = new HashMap<>();

    public static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // raw width and height
        final int width = options.outWidth;
        final int height = options.outHeight;
        int inSampleSize = 1;

        if (width > reqWidth || height > reqHeight) {
            final int halfWidth = width / 2;
            final int halfHeight = height / 2;

            while (halfWidth / inSampleSize > reqWidth && halfHeight / inSampleSize > reqHeight) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    public static Bitmap decodeSampleBitmap(String path, int reqWidth, int regHeight) {
        final BitmapFactory.Options options = new BitmapFactory.Options();


        //just calculate bound
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, options);

        options.inSampleSize = calculateInSampleSize(options, reqWidth, regHeight);
        // decode bitmap with bound which was calculated just now
        options.inJustDecodeBounds = false;

        return BitmapFactory.decodeFile(path, options);
    }

    public static Bitmap decodeSampleBitmap(byte[] data, int reqWidth, int regHeight, String hash) {
        final BitmapFactory.Options options = new BitmapFactory.Options();

        //just calculate bound
        options.inJustDecodeBounds = true;

        BitmapFactory.decodeByteArray(data, 0, data.length, options);

        optionsMap.put(hash, new int[]{options.outWidth, options.outHeight});

        Log.d("efficient", "w: " + options.outWidth + " h: " + options.outHeight);
        options.inSampleSize = calculateInSampleSize(options, reqWidth, regHeight);

        // decode bitmap with bound which was calculated just now
        options.inJustDecodeBounds = false;

        return BitmapFactory.decodeByteArray(data, 0, data.length, options);
    }

    public static Bitmap decodeSampleBitmap(Resources resources, int id, int reqWidth, int regHeight) {
        final BitmapFactory.Options options = new BitmapFactory.Options();

        //just calculate bound
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeResource(resources, id, options);

        options.inSampleSize = calculateInSampleSize(options, reqWidth, regHeight);

        // decode bitmap with bound which was calculated just now
        options.inJustDecodeBounds = false;

        return BitmapFactory.decodeResource(resources, id, options);
    }

    public int[] getBitmapOption(String tag) {
        return optionsMap.get(tag);
    }


}
