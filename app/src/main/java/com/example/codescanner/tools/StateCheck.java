package com.example.codescanner.tools;

/**
 * Created by Mac on 15/6/2.
 */
public class StateCheck {
    public interface StateListener {
        void onFail();
        void onLoad();
        void onPartialFail();
    }

    public static int INIT_STATE = 0;
    public static int FAIL_STATE = 1;
    public static int LOAD_STATE = 2;
    public static int PARTIAL_FAIL_STATE = 3;

    private int mCurrState;
    private StateListener mListener;

    public StateCheck() {
        mCurrState = INIT_STATE;
    }

    public void setStateListener(StateListener listener) {
        mListener = listener;
    }

    public int getState() {
        return mCurrState;
    }

    public void setState(boolean isSuccess) {
        if(mListener!=null) {
            if(isSuccess) {
                mCurrState = LOAD_STATE;
                mListener.onLoad();
            } else {
                if(mCurrState<2) {
                   mCurrState = FAIL_STATE;
                    mListener.onFail();
                } else {
                    mCurrState = PARTIAL_FAIL_STATE;
                    mListener.onPartialFail();
                }
            }
        }
    }
}
