package com.example.codescanner.tools;

/**
 * Created by Mac on 15/5/4.
 */
public interface FavoriteDataUtil extends Subject{
    FavoriteOption[] getOptions();
    void addFavorite(FavoriteOption option);
    void removeFavorite(FavoriteOption option);
    void addFavoritePoint(FavoriteOption option);
    void initStatus();
}
