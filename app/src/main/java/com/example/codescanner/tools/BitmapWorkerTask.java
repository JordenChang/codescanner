package com.example.codescanner.tools;

import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import java.lang.ref.WeakReference;

/**
 * Created by Mac on 15/3/8.
 */
public class BitmapWorkerTask {
    private static final String TAG = "Bitmap worker";
    private final WeakReference<ImageView> imageViewRef;
    private int viewWidth, width, height;
    private Callback mCallback;
    public interface Callback {
        void finish();
    }
    public BitmapWorkerTask(ImageView imageView) {
        // use weak reference to make sure that it can be garbage collected
        imageViewRef = new WeakReference<>(imageView);
        viewWidth = imageView.getWidth();
    }

    public void execute(String path, int w, int h) {
        width = w;
        height = h;
        new AsyncTask<String, Void, Bitmap>() {

            @Override
            protected Bitmap doInBackground(String... params) {
                Bitmap bitmap;
                if (height != -1 && width != -1) {
                    bitmap = EfficientBitmapHelper.decodeSampleBitmap(params[0], width, height);
                } else {
                    bitmap = EfficientBitmapHelper.decodeSampleBitmap(params[0], viewWidth, 0);
                }
                System.gc();
                return bitmap;
            }

            @Override
            protected void onPostExecute(Bitmap bitmap) {
                super.onPostExecute(bitmap);

                if (imageViewRef != null && bitmap != null) {
                    final ImageView imageView = imageViewRef.get();
                    if (imageView != null) {
                        Log.d(TAG, "compressed: " + bitmap);
                        imageView.setImageBitmap(bitmap);
                        if(mCallback!=null) {
                            mCallback.finish();
                        }
                        if (imageView.getVisibility() == View.INVISIBLE) {
                            imageView.setVisibility(View.VISIBLE);
                        }
                    }
                }

            }
        }.execute(path);
    }

    public void execute(byte[] source, int w, int h, final String key) {
        width = w;
        height = h;
        new AsyncTask<byte[], Void, Bitmap>() {
            @Override
            protected Bitmap doInBackground(byte[]... params) {
                final Bitmap bitmap = EfficientBitmapHelper.decodeSampleBitmap(params[0], width, height, key);

                System.gc();
                return bitmap;
            }

            @Override
            protected void onPostExecute(Bitmap bitmap) {
                super.onPostExecute(bitmap);
                if (bitmap != null) {
                    final ImageView imageView = imageViewRef.get();
                    if (imageView != null) {
                        imageView.setImageBitmap(bitmap);
                        if(mCallback!=null) {
                            mCallback.finish();
                        }
                        if (imageView.getVisibility() == View.INVISIBLE) {
                            imageView.setVisibility(View.VISIBLE);
                        }
                    }
                }
            }
        }.execute(source);
    }

    public void addCallback(Callback callback) {
        mCallback = callback;
    }

}
