package com.example.codescanner.tools;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.example.codescanner.R;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeSet;

/**
 * Created by Mac on 15/5/2.
 */
public class ManufactureDataUtil implements FavoriteDataUtil {
    private static final String TAG = "branch";
    private static Set<FavoriteOption> mMans = new TreeSet<>(new FavoriteOption.FavoriteComparator());
    private static List<Observer> mObservers = new ArrayList<>();
    private static SharedPreferences mFavoritePref;
    private static SharedPreferences.Editor mFavoriteEditor;
    private static boolean isInit;

    public ManufactureDataUtil(Context context) {
        if (!isInit) {
            // gets the preference for favorite branch
            if (mFavoritePref == null || mFavoriteEditor == null) {
                PreferenceHelper.init(context);
                mFavoritePref = PreferenceHelper.getManInfo();
                mFavoriteEditor = mFavoritePref.edit();
            }

            Scanner scanner = new Scanner(context.getResources().openRawResource(R.raw.manufactures), "big-5");

            while (scanner.hasNext()) {
                String name = scanner.nextLine();
                name = name.replaceAll("[,\"]", "");
                Log.d(TAG, name);
                mMans.add(new FavoriteOption(name,
                        mFavoritePref.getInt("i_" + name, 0),
                        mFavoritePref.getBoolean("b_" + name, false)));

            }
            isInit = true;
        }
    }

    /**
     * If branches is not sorted with favorite point, sort it and return.
     *
     * @return sorted branches list
     */
    public FavoriteOption[] getOptions() {
        return mMans.toArray(new FavoriteOption[mMans.size()]);
    }


    /**
     * Higher the favorite point of certain branch.
     *
     * @param option favorite manufacture
     */
    public void addFavoritePoint(FavoriteOption option) {
        int fav = mFavoritePref.getInt(option.getName(), 0) + 1;
        mFavoriteEditor.putInt("i_" + option.getName(), fav).apply();
        for (FavoriteOption o : mMans) {
            if (o.equals(option)) {
                o.setFavoritePoint(fav);
                Log.d(TAG, option.getName() + ":" + fav);
                break;
            }
        }

        notifyObserver();
    }

    public void addFavorite(FavoriteOption option) {
        Log.d(TAG, "fav: " + option.getName());
        mFavoriteEditor.putBoolean("b_" + option.getName(), true).apply();
        for (FavoriteOption o : mMans) {
            if (o.equals(option)) {
                o.setFavorite(true);
            }
        }
        notifyObserver();
    }

    public void removeFavorite(FavoriteOption option) {
        Log.d(TAG, "fav: " + option.getName());
        mFavoriteEditor.putBoolean("b_" + option.getName(), false).apply();
        for (FavoriteOption o : mMans) {
            if (o.equals(option)) {
                o.setFavorite(false);
            }
        }
        notifyObserver();
    }


    public void initStatus() {
        Log.d(TAG, "init");
        isInit = false;
        mFavoriteEditor.clear().apply();
        for (FavoriteOption o : mMans) {
            o.setFavorite(false);
            o.setFavoritePoint(0);
        }
        notifyObserver();
    }


    /**
     * Add observer.
     *
     * @param observer is new observer to observe branch data
     */
    @Override
    public void attach(Observer observer) {
        mObservers.add(observer);
    }

    /**
     * Remove observer.
     *
     * @param observer is not interested in this data any more.
     */
    @Override
    public void detach(Observer observer) {
        mObservers.remove(observer);
    }

    /**
     * Notifies all observer that data was changed.
     */
    @Override
    public void notifyObserver() {
        mMans = new TreeSet<>(mMans);
        int amount = mObservers.size();
        for (int i = 0; i < amount; i++) {
            mObservers.get(i).update(getOptions());
        }
    }
}
