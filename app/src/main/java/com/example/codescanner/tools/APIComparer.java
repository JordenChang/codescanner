package com.example.codescanner.tools;

import android.os.Build;

/**
 * Created by Mac on 15/2/21.
 */
public class APIComparer {
    public static boolean isGreaterOrEqual(int apiVersion) {
        return Build.VERSION.SDK_INT >= apiVersion;
    }
    public static boolean isLessThan(int apiVersion) {
        return Build.VERSION.SDK_INT < apiVersion;
    }
}
