package com.example.codescanner.tools;

/**
 * Created by Mac on 15/4/29.
 */
public interface Observer {
    void update(FavoriteOption[] data);
}
