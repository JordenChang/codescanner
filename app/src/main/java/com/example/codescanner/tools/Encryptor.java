package com.example.codescanner.tools;

/**
 * Created by Mac on 15/5/1.
 */
public class Encryptor {
    public static String encrypt(String s, String t) {
        StringBuilder sb = new StringBuilder();
        for(int i = 0; i < s.length(); i++)
            sb.append((char)(s.charAt(i) ^ t.charAt(i % t.length())));
        return sb.toString();
    }
}
