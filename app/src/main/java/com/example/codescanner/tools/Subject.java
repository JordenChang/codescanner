package com.example.codescanner.tools;

/**
 * Created by Mac on 15/4/29.
 */
public interface Subject {
    void attach(Observer observer);
    void detach(Observer observer);
    void notifyObserver();
}
