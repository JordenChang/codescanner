package com.example.codescanner.request.response;

/**
 * This listener only do something when request is successful.
 */
public abstract class SimpleResponseListener implements ResponseListener {

    @Override
    public void onClientError(String errorMsg) {
        //do nothing
    }

    @Override
    public void onServerError(String errorMsg) {
        //do nothing
    }

    @Override
    public void onNoResponse() {
    //do nothing
    }
}
