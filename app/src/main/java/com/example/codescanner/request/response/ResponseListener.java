package com.example.codescanner.request.response;

/**
 * Created by Mac on 15/5/3.
 */
public interface ResponseListener {
    void onSuccess(String result);
    void onClientError(String errorMsg);
    void onServerError(String errorMsg);
    void onNoResponse();
}
