package com.example.codescanner.request;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;

import com.example.codescanner.request.response.ResponseListener;
import com.example.codescanner.tools.PreferenceHelper;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Mac on 15/5/1.
 */
public class LoginUtil {
    private static final String TAG = "Login util";
    private static String URL = SendingRequestUtils.URL + "/erp/login";
    private SendingRequestUtils mRequestUtils;
    private ResponseListener mListener;
    private static String mId, mPwd;
    private Context mContext;

    public LoginUtil(Context context) {
        mRequestUtils = new SendingRequestUtils();
        mContext = context;
    }

    public void setLoginListener(ResponseListener loginListener) {
        mListener = loginListener;
    }

    public void login(String id, String pwd) {
        Log.d(TAG, "login: " + id);
        new AsyncTask<String, Void, String[]>() {
            @Override
            protected String[] doInBackground(String... params) {
                List<NameValuePair> pairs = new ArrayList<>();
                pairs.add(new BasicNameValuePair("id", params[0]));
                pairs.add(new BasicNameValuePair("password", params[1]));
                HttpResponse response = mRequestUtils.postToServer(URL, pairs);
                String result = "";
                String rc = "0";
                if (response != null) {
                    rc = String.valueOf(response.getStatusLine().getStatusCode());
                    try {
                        result = EntityUtils.toString(response.getEntity());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                return new String[]{rc, result};
            }

            @Override
            protected void onPostExecute(String[] results) {
                int rc = Integer.parseInt(results[0]);
                Log.d(TAG, rc + "/" + results[1]);
                if(rc==200) {
                    try {
                        JSONObject jResult = new JSONObject(results[1]);
                        String token = jResult.optString("token");
                        Log.d(TAG, "save token: " + token);
                        PreferenceHelper.getUserInfo().edit().putString("token", token).apply();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                callListener(rc, results[1]);


            }
        }.execute(id, pwd);
    }

    // call listener if it is not null
    private void callListener(int rc, String result) {
        if (mListener != null) {
            switch (rc) {
                case 0:
                    mListener.onNoResponse();
                    break;
                case 200:
                    mListener.onSuccess(result);
                    break;
                case 400:
                    mListener.onClientError(result);
                    break;
                case 500:
                    mListener.onServerError(result);
                    break;
            }
        }
    }

    public boolean reLogin() {
        if (mId != null && mPwd != null) {
            login(mId, mPwd);
            return true;
        } else {
            PreferenceHelper.init(mContext);
            SharedPreferences pref = PreferenceHelper.getUserInfo();
            mId = pref.getString("id", "");
            mPwd = pref.getString("password", "");

            if (!mId.isEmpty() && !mPwd.isEmpty()) {
                login(mId, mPwd);
                return true;
            }
        }

        return false;

    }
}
