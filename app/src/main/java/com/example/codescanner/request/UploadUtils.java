package com.example.codescanner.request;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.example.codescanner.request.response.ResponseListener;
import com.example.codescanner.tools.PreferenceHelper;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Mac on 15/5/1.
 */
public class UploadUtils implements ResponseListener {
    private static final String TAG = "Upload utils";
    private static String URL = SendingRequestUtils.URL + "/erp/album";
    private SendingRequestUtils mRequestUtils;
    private String mToken, mBranch, mMan, mLong, mLat, mSchedule;
    private String[] mPaths;
    private ProgressDialog mProgressDialog;
    private Context mContext;
    private int mFinish;
    private UploadCallback mCallback;

    @Override
    public void onSuccess(String result) {
        try {
            JSONObject jResult = new JSONObject(result);
            mToken = jResult.optString("token");
            PreferenceHelper.getUserInfo().edit().putString("token", mToken).apply();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        uploadImage(mPaths);
    }

    @Override
    public void onClientError(String errorMsg) {

    }

    @Override
    public void onServerError(String errorMsg) {
        Toast.makeText(mContext, "無法連線", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onNoResponse() {
        Toast.makeText(mContext, "無法連線", Toast.LENGTH_SHORT).show();
    }

    public interface UploadCallback {
        void onUploaded(String[] successName);

    }

    public  UploadUtils(Context context, String branch, String man, String schedule) {
        mContext = context;
        mRequestUtils = new SendingRequestUtils();
        PreferenceHelper.init(context);
        mToken = PreferenceHelper.getUserInfo().getString("token", "");
        mBranch = branch;
        mMan = man;
        mProgressDialog = new ProgressDialog(context);
        mProgressDialog.setMessage("上傳中...");
        mProgressDialog.setCancelable(false);
        mSchedule = schedule;
    }

    public void setUploadCallback(UploadCallback callback) {
        mCallback = callback;
    }

    public void uploadImage(String... paths) {
        Log.d(TAG, "upload: " + Arrays.toString(paths));
        mPaths = paths;
        mFinish = 0;

        mProgressDialog.setMax(paths.length);
        mProgressDialog.setProgress(0);
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        mProgressDialog.show();
        new AsyncTask<String, Integer, JSONObject>() {

            @Override
            protected JSONObject doInBackground(String... paths) {
                JSONArray failArray = new JSONArray();
                JSONArray successArray = new JSONArray();
                for (int i = 0; i < paths.length; i++) {
                    List<NameValuePair> params = new ArrayList<>();
                    params.add(new BasicNameValuePair("shopName", URLEncoder.encode(mBranch)));
                    params.add(new BasicNameValuePair("stockName", URLEncoder.encode(mMan)));
                    params.add(new BasicNameValuePair("image", paths[i]));
                    params.add(new BasicNameValuePair("schedule", mSchedule));

                    JSONObject jLocation = new JSONObject();
                    try {
                        jLocation.put("latitude", mLat);
                        jLocation.put("longitude", mLong);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    params.add(new BasicNameValuePair("location", jLocation.toString()));
                    Log.d(TAG, "upload with: " + params);
                    HttpResponse response = mRequestUtils.postToServer(URL + "?token=" + mToken, params);
                    publishProgress(i+1);
                    try {
                        if (response != null) {
                            int rc = response.getStatusLine().getStatusCode();
                            try {
                                Log.d(TAG, rc + ":" + EntityUtils.toString(response.getEntity()));
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            if(rc==400) {
                                // Cancel instantly, wait for user click next.
                                break;
                            }

                            // Puts path into success or fail array depending on rc.
                            if (rc == 200) {
                                Log.d(TAG, "success");
                                mFinish++;
                                successArray.put(paths[i]);
                            } else {

                                JSONObject jFail = new JSONObject();
                                jFail.put("failPath", paths[i]);
                                jFail.put("rc", rc);
                                failArray.put(jFail);
                            }
                        } else {
                            JSONObject jFail = new JSONObject();
                            jFail.put("failPath", paths[i]);
                            jFail.put("rc", 0);
                            failArray.put(jFail);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                JSONObject result = new JSONObject();
                try {
                    result.put("failArray", failArray);
                    result.put("successArray", successArray);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                return result;
            }

            @Override
            protected void onProgressUpdate(Integer... values) {
                super.onProgressUpdate(values);
                Log.d(TAG, "update: " + values[0]);
                mProgressDialog.setProgress(values[0]);
            }

            @Override
            protected void onPostExecute(JSONObject result) {
                super.onPostExecute(result);
                mProgressDialog.dismiss();
                JSONArray successArray = result.optJSONArray("successArray");
                JSONArray failArray = result.optJSONArray("failArray");
                Log.d(TAG, "result: " + result);
                // if there is no invalid token problem, the length of success array and fail array is equal to the length of all path.
                if(successArray!=null && failArray!=null && successArray.length()+failArray.length()==mPaths.length) {

                    Toast.makeText(mContext, "圖片上傳完畢, 成功" + mFinish + "張, 失敗" + (mPaths.length - mFinish) + "張", Toast.LENGTH_SHORT).show();
                    if (mCallback != null) {
                        // Sends the path of successfully posted picture to callback.

                        int len = successArray.length();
                        String[] successNames = new String[len];
                        for (int i = 0; i < len; i++) {
                            successNames[i] = successArray.optString(i);
                        }
                        mCallback.onUploaded(successNames);
                    }
                } else {
                    Log.d(TAG, "Invalid token");
                    reLogin();
                }
            }
        }.execute(paths);
    }

    public void reLogin() {
        LoginUtil loginUtil = new LoginUtil(mContext);
        loginUtil.setLoginListener(this);

        String id = PreferenceHelper.getUserInfo().getString("id", "");
        String pwd = PreferenceHelper.getUserInfo().getString("password", "");
        loginUtil.login(id, pwd);
    }

    public void setLocation(String latitude, String longitude) {
        mLat = latitude;
        mLong = longitude;
    }
}
