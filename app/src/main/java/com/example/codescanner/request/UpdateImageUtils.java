package com.example.codescanner.request;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import com.example.codescanner.tools.PreferenceHelper;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * This class send get for update image. When success Callback will be called and pass bitmap and
 * hash code of the bitmap. If a fail which caused by invalid token error is occur, involving login
 * to renew token.
 * Created by Mac on 15/2/27.
 */
public class UpdateImageUtils {
    private static final String TAG = "update image";
    private SendingRequestUtils mSendingRequestUtils;
    private Handler mHandler;
    private BlockingQueue<String> mQueue = new LinkedBlockingQueue<>();
    private Thread thread;
    private LoginUtil mLoginUtil;
    private int mTotal;
    volatile boolean isLoading;

    /**
     * Callback to pass bitmap and hash code.
     */

    public UpdateImageUtils(Context context, Handler handler) {
        mHandler = handler;
        mSendingRequestUtils = new SendingRequestUtils();
        mLoginUtil = new LoginUtil(context);
    }

    /**
     * @param hash identify image resource in server.
     */
    public void update(String hash) {
        mQueue.add(hash);
    }

    public void start() {
        mTotal = mQueue.size();
        Log.d(TAG, "start to update images: " + mTotal);

        final Timer timer = new Timer();
        // Checks if it is loading now every 5 second.
        TimerTask task = new TimerTask() {
            int count = 0;

            @Override
            public void run() {
                Log.d(TAG, "now loading: " + isLoading);
                if (!isLoading) {
                    if (!mQueue.isEmpty()) {
                        Log.d(TAG, "update: " + count);
                        update();
                        count++;
                        if (count >= mTotal) {
                            Log.d(TAG, "cancel");
                            timer.cancel();
                        }
                    } else {
                        Log.d(TAG, "cancel");
                        timer.cancel();
                    }
                }
            }
        };

        timer.scheduleAtFixedRate(task, 0, 5000);

    }

    /**
     * Sends get request to server to get image.
     */
    private void update() {
        isLoading = true;
        thread = new Thread(new Runnable() {
            @Override
            public void run() {
                String hash = null;
                try {

                    hash = mQueue.take();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                    thread.interrupt();
                }

                String url = SendingRequestUtils.URL + "/erp/files/" + hash + "?token=" + PreferenceHelper.getToken();
                HttpResponse httpResponse = mSendingRequestUtils.getFromServer(url);
                Bundle bundle = new Bundle();
                bundle.putString("hash", hash);
                try {

                    if (httpResponse != null) {

                        int rc = httpResponse.getStatusLine().getStatusCode();
                        Log.d(TAG, "response code: " + rc);
                        HttpEntity entity = httpResponse.getEntity();


                        if (rc == 200) {

                            InputStream is = entity.getContent();
                            ByteArrayOutputStream buffer = new ByteArrayOutputStream();

                            int nRead;
                            byte[] data = new byte[16384];

                            while ((nRead = is.read(data, 0, data.length)) != -1) {
                                buffer.write(data, 0, nRead);
                            }
                            buffer.flush();
                            is.close();


                            bundle.putByteArray("data", buffer.toByteArray());
                            bundle.putBoolean("success", true);
                            sendData(bundle);
                            Log.d(TAG, "put data to: " + hash);

                        } else if (rc == 400) {
                            isLoading = false;
                            bundle.putBoolean("success", false);
                            sendData(bundle);
                            mLoginUtil.reLogin();
                        } else if (rc == 500) {
                            isLoading = false;
                            bundle.putBoolean("success", false);
                            sendData(bundle);
                        }

                    } else {
                        bundle.putBoolean("success", false);
                        sendData(bundle);
                        isLoading = false;
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    bundle.putBoolean("success", false);
                    sendData(bundle);
                    isLoading = false;
                }
                Log.d(TAG, "rest: " + mQueue.size());
                if (mQueue.isEmpty()) {
                    isLoading = false;
                }
            }
        });
        thread.start();
    }

    public void interrupt() {
        Log.d(TAG, "interrupt update");
        if (thread != null) {
            thread.interrupt();
        }
        isLoading = false;
    }


    public synchronized boolean isLoading() {
        return isLoading;
    }

    public synchronized void setLoading(boolean isLoading) {
        this.isLoading = isLoading;
    }

    private void sendData(Bundle data) {
        Message msg = Message.obtain();
        msg.setData(data);
        mHandler.sendMessage(msg);
        if (!mQueue.isEmpty() && data.getBoolean("success")) {
            update();
        }
    }


}
