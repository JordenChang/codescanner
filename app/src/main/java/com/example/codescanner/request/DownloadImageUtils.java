package com.example.codescanner.request;

import android.content.Context;

import com.example.codescanner.request.response.ResponseListener;
import com.example.codescanner.tools.PreferenceHelper;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Mac on 15/5/6.
 */
public class DownloadImageUtils {
    private SendingRequestUtils mSendingRequestUtils;
    private Context mContext;
    private String mToken;
    public DownloadImageUtils(Context context) {
        mContext = context;
        mSendingRequestUtils = new SendingRequestUtils("/album");
        mToken = PreferenceHelper.getToken();
        mSendingRequestUtils.setUrlParam("token", mToken);

    }

    public void setResponseListener(ResponseListener listener) {
        mSendingRequestUtils.setResponseListener(listener);
    }

    public void setBeginning(String start, long since) {
        Map<String, String> params = new HashMap<>();
        params.put("start", start);
        params.put("since", String.valueOf(since));
        mSendingRequestUtils.setUrlParams(params);
    }

}
