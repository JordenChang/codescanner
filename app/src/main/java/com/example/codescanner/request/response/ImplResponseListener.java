package com.example.codescanner.request.response;

import android.content.Context;
import android.widget.Toast;

import com.example.codescanner.request.LoginUtil;

/**
 * Created by Mac on 15/5/8.
 */
public abstract class ImplResponseListener implements ResponseListener {
    private Context mContext;
    private LoginUtil mUtil;

    public ImplResponseListener(Context context) {
        this.mContext = context;
    }

    @Override
    public void onClientError(String errorMsg) {
        onFail();
        if (errorMsg.contains("token")) {
            mUtil = new LoginUtil(mContext);
            mUtil.reLogin();
        }

    }

    @Override
    public void onServerError(String errorMsg) {
        makeToast("無法連線");
        onFail();
    }

    @Override
    public void onNoResponse() {
        makeToast("無法連線");
        onFail();
    }

    public abstract void onFail();

    protected void makeToast(String msg) {
        Toast.makeText(mContext, msg, Toast.LENGTH_SHORT).show();
    }
}
