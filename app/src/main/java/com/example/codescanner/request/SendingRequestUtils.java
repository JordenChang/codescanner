package com.example.codescanner.request;

import android.os.AsyncTask;
import android.util.Log;

import com.example.codescanner.request.response.ResponseListener;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * Created by Mac on 15/2/15.
 */
public class SendingRequestUtils {
    public static final String URL = "http://199.101.117.251";
    public static final int HTTP_GET = 0;
    public static final int HTTP_POST = 1;
    public static final int HTTP_PUT = 2;
    public static final int HTTP_DEL = 3;
    private static final String TAG = "send request";
    private DefaultHttpClient mClient;
    private String mPath;
    private List<NameValuePair> mParams;
    private ResponseListener mListener;
    private StringBuilder mUrlParams;

    public SendingRequestUtils() {
        this(null);
    }

    public SendingRequestUtils(String path) {
        mPath = "/erp" + path;

        HttpParams params = new BasicHttpParams();
        HttpConnectionParams.setConnectionTimeout(params, 15000);
        mClient = new DefaultHttpClient(params);
    }

    public HttpResponse putToServer(String url, List<NameValuePair> params) {
        HttpPut put = new HttpPut(url);

        try {
            put.setEntity(new UrlEncodedFormEntity(params, "utf-8"));
            Log.i(TAG, "put to server: " + url + " with params: " + params);
            return mClient.execute(put);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public HttpResponse postToServer(String url, List<NameValuePair> params) {
        HttpPost post = new HttpPost(url);
        MultipartEntityBuilder builder = MultipartEntityBuilder.create();
        try {
            for (int i = 0; i < params.size(); i++) {
                String name = params.get(i).getName();
                String value = params.get(i).getValue();

                if (name.equals("image")) {
                    // value of image is path of file
                    File file = new File(value);
                    Log.d(TAG, file.toString());
                    builder.addBinaryBody(name, file);
                } else {
                    ContentType contentType = ContentType.create("text/plain", "utf-8");
                    builder.addTextBody(name, value, contentType);
                }
            }

            HttpEntity entity = builder.build();
            post.setEntity(entity);

            return mClient.execute(post);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;

    }


    public HttpResponse getFromServer(String url) {
        Log.i(TAG, "get from server:" + url);
        HttpGet get = new HttpGet(url);
        try {
            return mClient.execute(get);
        } catch (IOException e) {
            e.printStackTrace();

        }
        return null;
    }

    public HttpResponse deleteToServer(String url) {
        Log.i(TAG, "delete to server:" + url);
        HttpDelete delete = new HttpDelete(url);

        try {
            return mClient.execute(delete);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void execute(int type) {
        new AsyncTask<Integer, Void, String[]>() {
            @Override
            protected String[] doInBackground(Integer... params) {
                String url = URL + mPath;
                if (mUrlParams.length() > 1) {
                    // params is not empty.
                    url += mUrlParams.toString();
                }
                HttpResponse response = null;
                switch (params[0]) {
                    case HTTP_GET:
                        response = getFromServer(url);
                        break;
                    case HTTP_POST:
                        response = postToServer(url, mParams);
                        break;
                    case HTTP_PUT:
                        break;
                    case HTTP_DEL:
                        break;

                }
                String rc = "0", result = "";
                if (response != null) {
                    rc = String.valueOf(response.getStatusLine().getStatusCode());
                    try {
                        result = EntityUtils.toString(response.getEntity());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                return new String[]{rc, result};
            }

            @Override
            protected void onPostExecute(String[] results) {
                Log.d(TAG, results[0] + ":" + results[1]);
                if (mListener != null) {
                    switch (results[0].charAt(0)) {
                        case '0':
                            mListener.onNoResponse();
                            break;
                        case '2':
                            mListener.onSuccess(results[1]);
                            break;
                        case '4':
                            mListener.onClientError(results[1]);
                            break;
                        case '5':
                            mListener.onServerError(results[1]);
                            break;
                    }
                }
            }
        }.execute(type);

    }


    public void setParam(List<NameValuePair> param) {
        mParams = param;
    }

    public void setUrlParams(Map<String, String> param) {
        if (param.size() > 0) {
            mUrlParams = new StringBuilder("?");
            for (String key : param.keySet()) {
                mUrlParams.append(key).append("=").append(param.get(key)).append("&");
            }
            mUrlParams.deleteCharAt(mUrlParams.length() - 1);
        }
    }

    public void setUrlParam(String key, String value) {
        mUrlParams = new StringBuilder("?");
        mUrlParams
                .append(key)
                .append("=")
                .append(value);

    }

    public void setResponseListener(ResponseListener listener) {
        mListener = listener;
    }
}
