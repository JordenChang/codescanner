package com.example.codescanner;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.codescanner.request.SendingRequestUtils;
import com.example.codescanner.request.response.ImplResponseListener;
import com.example.codescanner.tools.APIComparer;
import com.example.codescanner.tools.BranchDataUtil;
import com.example.codescanner.tools.FavoriteOption;
import com.example.codescanner.tools.Observer;
import com.example.codescanner.tools.PreferenceHelper;
import com.example.codescanner.tools.RelaunchFlag;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Mac on 15/4/15.
 */
public class CheckInActivity extends ActionBarActivity implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener,
        GoogleMap.OnMyLocationButtonClickListener,
        OnMapReadyCallback, Observer {
    private static final String TAG = "CheckIn Activity";
    private static final LocationRequest REQUEST = LocationRequest.create()
            .setInterval(5000)
            .setFastestInterval(60)
            .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    private TextView mAddressText;
    private GoogleApiClient mGoogleClient;
    private long mUpdateTime;
    private GoogleMap mMap;
    private Location mCurrentLocation;
    private boolean isPrepare;
    private SendingRequestUtils mUtils;
    private FavoriteOption[] mBranches;
    private ArrayAdapter<FavoriteOption> mBranchAdapter;
    private Spinner mSpinner;
    private ProgressDialog mDialog;
    private int mSelect;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_in);
RelaunchFlag.getInstance().setIsNeed(false);
        mAddressText = (TextView) findViewById(R.id.address_text);
        mDialog = new ProgressDialog(this);
        mDialog.setMessage("處理中...");
        mDialog.setCancelable(false);

        // init map fragment
        SupportMapFragment map = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        map.getMapAsync(this);


        mUtils = new SendingRequestUtils("/locations");
        mUtils.setUrlParam("token", PreferenceHelper.getToken());
        mUtils.setResponseListener(new ImplResponseListener(this) {
            @Override
            public void onFail() {
                dismissProgress();
            }

            @Override
            public void onSuccess(String result) {
                PreferenceHelper.getUserInfo().edit().putInt("location_index", mSelect).apply();
                dismissProgress();
                Toast.makeText(CheckInActivity.this, "簽到完成", Toast.LENGTH_SHORT).show();
            }
        });


        mGoogleClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();


    }

    @SuppressLint("NewApi")
    private void initSpinner() {
        if (APIComparer.isGreaterOrEqual(16)) {
            mSpinner.setPopupBackgroundDrawable(new ColorDrawable(Color.WHITE));
        }
        BranchDataUtil mDataUtils = new BranchDataUtil(this);
        mDataUtils.attach(this);
        mBranches = mDataUtils.getOptions();
        Log.d(TAG, "get: " + Arrays.toString(mBranches));
        mBranchAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, android.R.id.text1, mBranches);
        mBranchAdapter.setDropDownViewResource(android.R.layout.simple_list_item_1);
        mSpinner.setAdapter(mBranchAdapter);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mGoogleClient.connect();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mGoogleClient.disconnect();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.check_in, menu);

        View v = menu.findItem(R.id.menu_spinner1).getActionView();
        Log.d(TAG, "menu item: " + v);
        if (v instanceof Spinner) {
            mSpinner = (Spinner) v;
            initSpinner();
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.check_in:

                sendLocation();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    private void sendLocation() {
        JSONObject location = new JSONObject();
        if (isPrepare) {
            mDialog.show();
            try {
                 mSelect = mSpinner.getSelectedItemPosition();
                location.put("latitude", mCurrentLocation.getLatitude());
                location.put("longitude", mCurrentLocation.getLongitude());
                location.put("shopName", URLEncoder.encode(mBranches[mSelect].getName()));

                List<NameValuePair> params = new ArrayList<NameValuePair>();
                Log.d(TAG, "check info: " + location.toString());
                params.add(new BasicNameValuePair("location", location.toString()));
                mUtils.setParam(params);

                mUtils.execute(SendingRequestUtils.HTTP_POST);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            Toast.makeText(this, "定位中...", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        Log.d(TAG, "on map ready");
        mMap = googleMap;
        googleMap.setMyLocationEnabled(true);
        googleMap.getUiSettings().setMyLocationButtonEnabled(true);

    }

    @Override
    public boolean onMyLocationButtonClick() {
        return true;
    }

    @Override
    public void onConnected(Bundle bundle) {
        Log.d(TAG, "connected");
        LocationServices.FusedLocationApi.requestLocationUpdates(
                mGoogleClient,
                REQUEST,
                this);  // LocationListener
    }

    @Override
    public void onConnectionSuspended(int i) {
        // do nothing
    }

    @Override
    public void onLocationChanged(Location location) {
        Log.d(TAG, "location" + location);

        long newTime = location.getTime();
        // update location every 3 min.
        if (newTime - mUpdateTime > 180000) {
            mCurrentLocation = location;
            isPrepare = true;
            // moving the camera when location update
            if (mMap != null) {
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(
                        new LatLng(location.getLatitude(), location.getLongitude()), 13));
            }

            mUpdateTime = newTime;
            Geocoder geocoder = new Geocoder(this);
            try {
                List<Address> address = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
                mAddressText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
                mAddressText.setText(address.get(0).getAddressLine(0));

            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }


    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        // do nothing
    }


    @Override
    public void update(FavoriteOption[] data) {
        mBranches = data;
        mBranchAdapter.sort(new FavoriteOption.FavoriteComparator());
        mBranchAdapter.notifyDataSetChanged();
    }

    public void dismissProgress() {
        try {
            if (mDialog != null && mDialog.isShowing()) {
                mDialog.dismiss();
            }
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
    }
}
