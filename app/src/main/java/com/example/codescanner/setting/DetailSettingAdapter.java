package com.example.codescanner.setting;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import com.example.codescanner.R;
import com.example.codescanner.tools.FavoriteOption;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Mac on 15/5/4.
 */
public class DetailSettingAdapter extends RecyclerView.Adapter<DetailSettingAdapter.MyViewHolder> {

    private static final String TAG = "detail adapter";

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        public CheckBox checkText;

        public MyViewHolder(View itemView) {
            super(itemView);
            checkText = (CheckBox) itemView.findViewById(R.id.check_text);

        }
    }

    private Set<Integer> mCheckPos;
    private FavoriteOption[] mSettingNames;

    public DetailSettingAdapter(FavoriteOption[] options) {
        Log.d(TAG, "options: " + Arrays.toString(options));
        mCheckPos = new HashSet<>();
        mSettingNames = options;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_setting_item, parent, false);
        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        FavoriteOption favoriteOption = mSettingNames[position];
        Log.d(TAG, favoriteOption.getName() + ":" + favoriteOption.getFavoritePoint()+"("+favoriteOption.isFavorite()+")");
        holder.checkText.setText(favoriteOption.getName() + ":" + favoriteOption.getFavoritePoint() + "次");
        holder.checkText.setChecked(favoriteOption.isFavorite());

        holder.checkText.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    Log.d(TAG, "add: " + position);
                    mCheckPos.add(position);
                } else {
                    Log.d(TAG, "remove:" + position);
                    mCheckPos.remove(Integer.valueOf(position));
                }
            }
        });
        Log.d(TAG, "check: " + mCheckPos);
    }

    @Override
    public int getItemCount() {
        return mSettingNames.length;
    }

    public Set<Integer> getCheckPos() {
        return mCheckPos;
    }
}
