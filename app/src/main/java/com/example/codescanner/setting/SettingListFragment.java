package com.example.codescanner.setting;

import android.support.v4.app.ListFragment;
import android.view.View;
import android.widget.ListView;

/**
 * Created by Mac on 15/5/3.
 */
public class SettingListFragment extends ListFragment {
    public interface Callback {
        void onItemClick(int position);
    }

    private Callback callback;

    public void setCallback(Callback callback) {
        this.callback = callback;
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        if(callback!=null) {
            callback.onItemClick(position);
        }
    }


}
