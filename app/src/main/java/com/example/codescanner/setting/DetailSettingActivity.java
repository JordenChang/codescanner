package com.example.codescanner.setting;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.example.codescanner.R;
import com.example.codescanner.tools.BranchDataUtil;
import com.example.codescanner.tools.FavoriteDataUtil;
import com.example.codescanner.tools.FavoriteOption;
import com.example.codescanner.tools.ManufactureDataUtil;
import com.example.codescanner.tools.Observer;

/**
 * Created by Mac on 15/5/3.
 */
public class DetailSettingActivity extends ActionBarActivity implements View.OnClickListener, Observer {
    private static final String TAG = "detail setting";
    private int mListType = 1;
    private RecyclerView mSettingList;
    private DetailSettingAdapter mSettingAdapter;
    private FavoriteDataUtil mDataUtil;
    private FavoriteOption[] mOptions;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_setting);

        mSettingList = (RecyclerView) findViewById(R.id.setting_list);
        Button mConfirmBtn = (Button) findViewById(R.id.confirm_btn);
        Button mInitBtn = (Button) findViewById(R.id.init_btn);

        mInitBtn.setOnClickListener(this);
        mConfirmBtn.setOnClickListener(this);
        mSettingList.setHasFixedSize(true);

        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);

        Intent preIntent = getIntent();
        if (preIntent != null) {
            mListType = preIntent.getIntExtra("type", 1);
        }
        Log.d(TAG, "type: " + mListType);
        switch (mListType) {
            case 1:
                mDataUtil = new BranchDataUtil(this);
                break;
            case 2:
                mDataUtil = new ManufactureDataUtil(this);
                break;
        }

        mOptions = mDataUtil.getOptions();
        mDataUtil.attach(this);
        mSettingAdapter = new DetailSettingAdapter(mOptions);

        mSettingList.setAdapter(mSettingAdapter);
        mSettingList.setLayoutManager(mLayoutManager);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.confirm_btn:
                favor();
                break;
            case R.id.init_btn:
                init();
                break;
        }
    }

    private void favor() {
        for (Integer i : mSettingAdapter.getCheckPos()) {
            mDataUtil.addFavorite(mOptions[i]);
        }
    }

    private void init() {
        new AlertDialog.Builder(this)
                .setMessage("是否清除所有次數？")
                .setTitle("初始化")
                .setNegativeButton("取消", null)
                .setPositiveButton("確定", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mDataUtil.initStatus();
                    }
                }).show();
    }

    @Override
    public void update(FavoriteOption[] data) {
        mOptions = data;
        mSettingAdapter = new DetailSettingAdapter(data);
        mSettingList.swapAdapter(mSettingAdapter, true);
        mSettingAdapter.notifyDataSetChanged();
    }
}
