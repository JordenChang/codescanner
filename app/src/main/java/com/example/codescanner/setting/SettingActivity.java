package com.example.codescanner.setting;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.widget.ArrayAdapter;

import com.example.codescanner.LoginActivity;
import com.example.codescanner.R;
import com.example.codescanner.tools.PreferenceHelper;

/**
 * Created by Mac on 15/4/15.
 */
public class SettingActivity extends ActionBarActivity implements SettingListFragment.Callback, DialogInterface.OnClickListener {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);

        if (savedInstanceState == null) {
            String[] settings = getResources().getStringArray(R.array.user_settings);
            SettingListFragment fragment = new SettingListFragment();
            fragment.setCallback(this);
            fragment.setListAdapter(new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, android.R.id.text1, settings));
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.container, fragment)
                    .commit();
        }
    }

    @Override
    public void onItemClick(int position) {
        switch (position) {
            case 0:
                logout();
                break;
            case 1:
//                startActivity(DetailSettingActivity.class, 2);
                break;
            case 2:

                break;
        }
    }

    private void logout() {

        new AlertDialog.Builder(this)
                .setTitle("登出")
                .setMessage("確定登出？")
                .setPositiveButton("確定", this)
                .setNegativeButton("取消", this)
                .create()
                .show();
    }

    private void startActivity(Class clazz, int type) {
        Intent intent = new Intent(this, clazz);
        intent.putExtra("type", type);
        startActivity(intent);
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        if (which == -1) {
            PreferenceHelper.getUserInfo().edit().putBoolean("isLogin", false).apply();
            Intent intent = new Intent(this, LoginActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.putExtra("logout", true);
            startActivity(intent);
        }
    }
}
