package com.example.codescanner;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.location.Criteria;
import android.location.Location;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.Toast;

import com.example.codescanner.image.Action;
import com.example.codescanner.image.ImageFileCreator;
import com.example.codescanner.request.SendingRequestUtils;
import com.example.codescanner.request.UploadUtils;
import com.example.codescanner.tools.BitmapWorkerTask;
import com.example.codescanner.tools.BranchDataUtil;
import com.example.codescanner.tools.FavoriteOption;
import com.example.codescanner.tools.ManufactureDataUtil;
import com.example.codescanner.tools.Observer;
import com.example.codescanner.tools.PreferenceHelper;
import com.example.codescanner.tools.RelaunchFlag;
import com.example.codescanner.tools.SizeHelper;
import com.example.codescanner.widget.ImageViewer;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.io.File;
import java.io.FileFilter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by Mac on 15/4/14.
 */
public class UploadFragment extends Fragment implements View.OnClickListener, UploadUtils.UploadCallback {
    private static final String TAG = "Upload fragment";
    private static final int CAPTURE_CAMERA = 10;
    private static final int CAPTURE_IMAGE = 20;
    private Uri mImageCaptureUri;
    private ViewGroup mThumbnailList;
    private ArrayAdapter<FavoriteOption> mBranchAdapter, mManAdapter;
    private FavoriteOption[] mBranches,
            mMans;
    private UploadUtils mUploadUtils;
    private Spinner mBranchSpinner, mManSpinner;
    private boolean mCompressFinish, mIsGetLocation, mIsLastMonth;
    private int mCompressNum, mCounter, mMonth;
    private ProgressDialog mLoadingDialog;
    private Location mLocation;
    private Handler mHandler;
    private RadioGroup mScheduleGroup;
    private View mView;
    private Switch mLastMonth;
    private RadioButton mSchedule1, mSchedule2;
    private ArrayList<String> mPaths;

    @SuppressLint("NewApi")
    @Override
    public void onAttach(final Activity activity) {
        super.onAttach(activity);

        BranchDataUtil mBranchUtil = new BranchDataUtil(activity);
        mBranchUtil.attach(new Observer() {
            @Override
            public void update(FavoriteOption[] data) {
                mBranches = data;
                mBranchAdapter.sort(new FavoriteOption.FavoriteComparator());
                mBranchAdapter.notifyDataSetChanged();
            }
        });

        ManufactureDataUtil mManUtil = new ManufactureDataUtil(activity);
        mManUtil.attach(new Observer() {
            @Override
            public void update(FavoriteOption[] data) {
                mMans = data;
                mManAdapter.sort(new FavoriteOption.FavoriteComparator());
                mManAdapter.notifyDataSetChanged();
            }
        });

        //get branch data
        mBranches = mBranchUtil.getOptions();

        mMans = mManUtil.getOptions();
        final ProgressDialog dialog = new ProgressDialog(activity);
        dialog.setMessage("準備中...");
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                File store = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
                store = new File(store, "連興行銷");
                // Delete compressed image files
                Log.d(TAG, "store: " + store);
                if (store.isDirectory()) {
                    FileFilter filter = new FileFilter() {
                        @Override
                        public boolean accept(File pathname) {
                            String path = pathname.getPath();
                            return path.contains("CMP_");
                        }
                    };
                    File[] files = store.listFiles(filter);
                    Log.d(TAG, "Delete cmp files: " + Arrays.toString(files));
                    if (files != null) {
                        for (File file : files) {
                            file.delete();
                        }
                        MediaScannerConnection.scanFile(activity, new String[]{Environment.getExternalStorageDirectory().toString()}, null, new MediaScannerConnection.OnScanCompletedListener() {
                            /*
                             *   (non-Javadoc)
                             * @see android.media.MediaScannerConnection.OnScanCompletedListener#onScanCompleted(java.lang.String, android.net.Uri)
                             */
                            public void onScanCompleted(String path, Uri uri) {
                                Log.i("ExternalStorage", "Scanned " + path + ":");
                                Log.i("ExternalStorage", "-> uri=" + uri);
                            }
                        });
                    }
                    Log.d(TAG, "delete finish");
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                dialog.dismiss();
            }
        }.execute();
        dialog.show();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mHandler = new Handler();
        mPaths = new ArrayList<>();
        setHasOptionsMenu(true);
        mIsGetLocation = false;
        Criteria criteria = new Criteria();
        criteria.setAccuracy(Criteria.ACCURACY_FINE);

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_upload, container, false);
        mBranchSpinner = (Spinner) mView.findViewById(R.id.branch_spinner);
        mManSpinner = (Spinner) mView.findViewById(R.id.manufacture_spinner);
        mScheduleGroup = (RadioGroup) mView.findViewById(R.id.schedule);
        mSchedule1 = (RadioButton) mView.findViewById(R.id.schedule1);
        mSchedule2 = (RadioButton) mView.findViewById(R.id.schedule2);

        mLastMonth = (Switch) mView.findViewById(R.id.is_last_month);
        mIsLastMonth = PreferenceHelper.getLastMonth();
        mLastMonth.setChecked(mIsLastMonth);

        Date date = new Date();
        SimpleDateFormat smf = new SimpleDateFormat("M");

        mMonth = Integer.valueOf(smf.format(date));
        checkLastMonth();

        mLastMonth.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Log.d(TAG, "switch: " + isChecked);
                mIsLastMonth = isChecked;
                checkLastMonth();
            }
        });


        mThumbnailList = (ViewGroup) mView.findViewById(R.id.thumbnail_list);
        ImageButton addPicBtn = (ImageButton) mView.findViewById(R.id.add_pic_btn);
        ImageButton cameraBtn = (ImageButton) mView.findViewById(R.id.camera_btn);

        mLoadingDialog = new ProgressDialog(getActivity());
        mLoadingDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        mLoadingDialog.setMessage("載入中...");
        mLoadingDialog.setCancelable(false);

        mBranchAdapter = new ArrayAdapter<>(getActivity()
                , android.R.layout.simple_list_item_1, android.R.id.text1, mBranches);
        mManAdapter = new ArrayAdapter<>(getActivity()
                , android.R.layout.simple_list_item_1, android.R.id.text1, mMans);


        mBranchSpinner.setAdapter(mBranchAdapter);
        mManSpinner.setAdapter(mManAdapter);

        // If branch has been changed, saved index is invalid and should be reset.
        int location = PreferenceHelper.getUserInfo().getInt("location_index", 0);
        if(location<mBranchAdapter.getCount()) {
            mBranchSpinner.setSelection(location);
        } else {
            mBranchSpinner.setSelection(0);
            PreferenceHelper.getUserInfo().edit().putInt("location_index", 0).apply();

        }

//        Log.d(TAG, "set selection: "+ );
        addPicBtn.setOnClickListener(this);
        cameraBtn.setOnClickListener(this);
        return mView;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.upload, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.upload_btn:
                if (mScheduleGroup.getCheckedRadioButtonId() == -1) {
                    Toast.makeText(getActivity(), "請選擇檔期", Toast.LENGTH_SHORT).show();
                } else {
                    PreferenceHelper.getUserInfo().edit().putBoolean("last_month", mIsLastMonth).apply();
                    upload();
                }
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        RelaunchFlag.getInstance().setIsNeed(false);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == CAPTURE_CAMERA) {
                if (mImageCaptureUri != null) {
                    String path = mImageCaptureUri.getPath();

                    mCompressFinish = false;
                    mCompressNum += 1;
                    addThumbnailToList(path);
                }
            } else if (requestCode == CAPTURE_IMAGE) {
                String[] paths = data.getStringArrayExtra("all_path");
                addThumbnailsToList(paths);
                Log.d(TAG, "paths:" + Arrays.toString(paths));
            }


        }
    }

    public void takePicture() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        mImageCaptureUri = ImageFileCreator.getOutputMediaFileUri();
//        intent.putExtra(MediaStore.EXTRA_SCREEN_ORIENTATION, ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        Log.d(TAG, "get uri: " + mImageCaptureUri);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, mImageCaptureUri);
        startActivityForResult(intent, CAPTURE_CAMERA);

    }


    @Override
    public void onViewStateRestored(Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        if (savedInstanceState != null && savedInstanceState.containsKey("cameraImageUri")) {
            mImageCaptureUri = Uri.parse(savedInstanceState.getString("cameraImageUri"));
            Log.d(TAG, "restore: " + mImageCaptureUri);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        Log.d(TAG, "save uri");
        if (mImageCaptureUri != null) {
            outState.putString("cameraImageUri", mImageCaptureUri.toString());
        }
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.camera_btn:
                takePicture();
                break;
            case R.id.add_pic_btn:
                Intent intent = new Intent(Action.ACTION_MULTIPLE_PICK);
                intent.putExtra("limit", -1);
                startActivityForResult(intent, CAPTURE_IMAGE);
                break;
            case R.id.cancel_img:
                mThumbnailList.removeView(mThumbnailList.findViewWithTag(v.getTag()));
                mPaths.remove(v.getTag());
                mCompressNum--;
                break;
            case R.id.content_img:
                Log.d(TAG, "tag:" + v.getTag());
                String tag = (String) v.getTag();
                if (!tag.startsWith("$")) {
                    // Show image viewer
                    ImageViewer viewer = new ImageViewer(getActivity());
                    viewer.show((String) v.getTag(), mPaths);
                } else {
                    // Fail to compress, retry.
                    mThumbnailList.removeView(mThumbnailList.findViewWithTag(tag));
                    mCompressNum--;
                    mPaths.remove(v.getTag());
                    addThumbnailToList(tag);
                }
                break;
        }
    }

    public void addThumbnailToList(String path) {


        final View cancelImage = View.inflate(getActivity(), R.layout.layout_cancelable_image, null);
        final ImageButton cancel = (ImageButton) cancelImage.findViewById(R.id.cancel_img);
        final ImageView content = (ImageView) cancelImage.findViewById(R.id.content_img);

        content.setOnClickListener(this);
        cancel.setOnClickListener(UploadFragment.this);


        new AsyncTask<String, Integer, String>() {


            @Override
            protected String doInBackground(String... params) {
                File cmpFile = ImageFileCreator.compressBitmap(params[0]);
                publishProgress(++mCounter);
                return cmpFile == null ? "$" + params[0] : cmpFile.toString();
            }

            @Override
            protected void onProgressUpdate(Integer... values) {
                super.onProgressUpdate(values);
                mLoadingDialog.setProgress(values[0]);
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);

                cancel.setTag(s);
                cancelImage.setTag(s);
                content.setTag(s);

                if (!s.startsWith("$")) {
                    mPaths.add(s);
                    int size[] = SizeHelper.getScreenSize(getActivity());
                    BitmapWorkerTask task = new BitmapWorkerTask(content);
                    task.addCallback(new BitmapWorkerTask.Callback() {
                        @Override
                        public void finish() {

                            if (mCounter >= mCompressNum) {
                                if (mLoadingDialog.isShowing()) {
                                    mLoadingDialog.dismiss();
                                }
                                mCompressFinish = true;
                            }
                        }
                    });
                    task.execute(s, size[0] / 10, size[1] / 10);
                }

            }
        }.execute(path);

        mThumbnailList.addView(cancelImage);


    }

    public void addThumbnailsToList(String[] paths) {
        Log.d(TAG, "add pic:" + Arrays.toString(paths));
        mCompressFinish = false;
        mCompressNum = paths.length;
        mLoadingDialog.setMax(mCompressNum);
        mLoadingDialog.show();
        for (String s : paths) {
            addThumbnailToList(s);
        }
    }

    public void upload() {
        Log.d(TAG, "compress# " + mCompressNum);
        if (mCompressNum > 0) {
            if (mCompressFinish) {
                final ProgressDialog dialog = new ProgressDialog(getActivity());
                dialog.setMessage("定位中...");
                dialog.setCancelable(false);
                String schedule = (String) ((RadioButton) mView.findViewById(mScheduleGroup.getCheckedRadioButtonId())).getText();
                mUploadUtils = new UploadUtils(getActivity(),
                        mBranches[mBranchSpinner.getSelectedItemPosition()].getName(),
                        mMans[mManSpinner.getSelectedItemPosition()].getName(), schedule);
                mUploadUtils.setUploadCallback(this);
                int size = mThumbnailList.getChildCount();
                final String[] tags = new String[size];
                for (int i = 0; i < size; i++) {
                    tags[i] = (String) mThumbnailList.getChildAt(i).getTag();
                }
                Log.d(TAG, "test first: " + System.currentTimeMillis());
                dialog.show();
                if (mIsGetLocation) {
                    mUploadUtils.setLocation(String.valueOf(mLocation.getLatitude()), String.valueOf(mLocation.getLongitude()));
                    dialog.dismiss();
                    mUploadUtils.uploadImage(tags);

                } else {
                    final Timer timer = new Timer();
                    TimerTask task = new TimerTask() {
                        private int counter;

                        @Override
                        public void run() {
                            Log.d(TAG, "test" + counter + ": " + System.currentTimeMillis());
                            mHandler.post(new Runnable() {
                                @Override
                                public void run() {
                                    if (counter < 15) {
                                        if (mIsGetLocation) {
                                            mUploadUtils.setLocation(String.valueOf(mLocation.getLatitude()), String.valueOf(mLocation.getLongitude()));
                                            mUploadUtils.uploadImage(tags);
                                            timer.cancel();
                                            dialog.dismiss();
                                        }
                                    } else {
                                        mUploadUtils.setLocation("0", "0");
                                        mUploadUtils.uploadImage(tags);
                                        timer.cancel();
                                        dialog.dismiss();
                                    }
                                }
                            });

                            counter++;


                        }
                    };
                    timer.schedule(task, 0, 1000);
                }
            } else {
                Toast.makeText(getActivity(), "資料載入中...", Toast.LENGTH_SHORT).show();
            }
        }
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        // Clear all compressed temp image file.

    }

    @Override
    public void onUploaded(String[] names) {
        // Removes pic from list if it's uploaded successfully.
        Log.d(TAG, "success# " + names.length);
        if (names.length > 0) {
            for (String name : names) {
                Log.d(TAG, "remove: " + name);
                mThumbnailList.removeView(mThumbnailList.findViewWithTag(name));
                mCompressNum--;
            }

            new AsyncTask<String, Void, Void>() {
                @Override
                protected Void doInBackground(String... params) {
                    // Delete compressed image files
                    for (String name : params) {
//                        getActivity().getContentResolver().delete(Uri.fromFile(file), null, null);
                        Log.d(TAG, "delete file: " + name);
                        File file = new File(name);
                        file.delete();
                    }
                    MediaScannerConnection.scanFile(getActivity(), new String[]{Environment.getExternalStorageDirectory().toString()}, null, new MediaScannerConnection.OnScanCompletedListener() {
                        /*
                         *   (non-Javadoc)
                         * @see android.media.MediaScannerConnection.OnScanCompletedListener#onScanCompleted(java.lang.String, android.net.Uri)
                         */
                        public void onScanCompleted(String path, Uri uri) {
                            Log.i("ExternalStorage", "Scanned " + path + ":");
                            Log.i("ExternalStorage", "-> uri=" + uri);
                        }
                    });
                    return null;
                }
            }.execute(names);

            // Send notification to server indicate upload is finish.
            SendingRequestUtils utils = new SendingRequestUtils("/notifications");
            List<NameValuePair> params = new ArrayList<>();

            params.add(new BasicNameValuePair("shopName", mBranches[mBranchSpinner.getSelectedItemPosition()].getName()));
            params.add(new BasicNameValuePair("manName", mMans[mManSpinner.getSelectedItemPosition()].getName()));
            params.add(new BasicNameValuePair("numbers", String.valueOf(names.length)));
            utils.setParam(params);
            utils.setUrlParam("token", PreferenceHelper.getToken());
            utils.execute(SendingRequestUtils.HTTP_POST);
        }
    }

    public void setLocation(Location location) {
        Log.d(TAG, "location set:" + location);
        mIsGetLocation = true;
        mLocation = location;
    }

    private void checkLastMonth() {
        int month = mIsLastMonth ? mMonth - 1 : mMonth;
        mSchedule1.setText(month + "-1");
        mSchedule2.setText(month + "-2");
    }

}
